#include "shortcutresolver.hpp"


namespace wgt {
ShortcutResolver::Ptree* ShortcutResolver::getKeymap() const {
    return keymap.get();
}

void ShortcutResolver::setKeymap(Ptree* value) {
    keymap.reset(value);
}

ShortcutResolver::ShortcutResolver() : keymap(new Ptree) {
    this->delimiter = "+";
}

QString ShortcutResolver::getDelimiter() const {
    return delimiter;
}

void ShortcutResolver::setDelimiter(const QString& value) {
    delimiter = value;
}

void ShortcutResolver::addBinding(QString shortcut, QString action) {
    this->addBinding(shortcut.toStdString(), action.toStdString());
}

void ShortcutResolver::addBinding(
    const char* shortcut,
    const char* action) {
    this->addBinding(std::string(shortcut), std::string(action));
}

void ShortcutResolver::addBinding(
    std::string shortcut,
    std::string action) {
    KBD_LOG << shortcut << action;
    std::string path = toPath(shortcut).toStdString();
    keymap->put(path, action);
}

QString ShortcutResolver::getCommand(QString shortcut) {
    return this->getCommand(shortcut.toStdString());
}

QString ShortcutResolver::getCommand(const KeySeqence& sequence) {
    return this->getCommand(sequence.toString());
}

QString ShortcutResolver::getCommand(std::string shortcut) {
    std::string path = toPath(shortcut).toStdString();
    try {
        return QString::fromStdString(keymap->get<std::string>(path));
    } catch (...) { return ""; }
}

/*!
 * \todo WTF?
 * \todo Possible to replace with range-for loop instead of index-based one
 * \warning Changed after 548f4d12a5cc82f4c5b214383a9b22f01e30c69e but not
 * tested at all.
 */
std::vector<ShortcutResolver::binding> ShortcutResolver::getChildren(
    Ptree& tree) {
    std::vector<binding> result;

    for (Ptree::iterator iter = tree.begin(); iter != tree.end(); ++iter) {
        std::string key       = (*iter).first;
        Ptree       childTree = (*iter).second;
        std::string value     = boost::lexical_cast<std::string>(
            childTree.get_value(key));

        if (childTree.size() == 0) {
            result.emplace_back(key, value);
        } else {
            result.emplace_back(key, value);
            std::vector<binding> bindings = getChildren((*iter).second);
            for (const binding& bind : bindings) {
                result.emplace_back(
                    key + delimiter.toStdString() + bind.first,
                    bind.second);
            }
        }
    }

    return result;
}

QString ShortcutResolver::toPath(QString shortcut) {
    return standartiseShortcut(shortcut).split(delimiter).join(".");
}

QString ShortcutResolver::toPath(std::string shortcut) {
    return (standartiseShortcut(QString::fromStdString(shortcut)))
        .split(delimiter)
        .join(".");
}

QVector<QPair<QString, QString>> ShortcutResolver::getCompletion(
    std::string shortcut) {
    std::string path = ((QString::fromStdString(shortcut))
                            .split("+")
                            .join("."))
                           .toStdString();
    Ptree                            completion = keymap->get_child(path);
    std::vector<binding>             result     = getChildren(completion);
    QVector<QPair<QString, QString>> out;
    for (const binding& bind : result) {
        out.push_back(QPair<QString, QString>(
            QString::fromStdString(bind.first),
            QString::fromStdString(bind.second)));
    }
    return out;
}

QString ShortcutResolver::standartiseShortcut(std::string& shortcut) {
    return standartiseShortcut(QString::fromStdString(shortcut));
}

QString ShortcutResolver::standartiseShortcut(QString shortcut) {
    QStringList result;
    QStringList input = shortcut.toLower().split(delimiter);
    input.removeDuplicates();
    result.append((input.removeOne("ctrl") ? "ctrl" : ""));
    result.append((input.removeOne("shift") ? "shift" : ""));
    result.append((input.removeOne("alt") ? "alt" : ""));
    result.append((input.removeOne("win") ? "win" : ""));
    result.append(input.last());
    return result.join(delimiter);
}

void ShortcutResolver::printTree() {
    boost::property_tree::info_parser::write_info(std::cout, *keymap);
    std::cout << std::endl;
}
} // namespace wgt
