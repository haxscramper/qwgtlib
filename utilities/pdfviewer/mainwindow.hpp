#ifndef MAINWINDOW_HPP
#define MAINWINDOW_HPP

#include <qwgtlib/filetreeviewer.hpp>
#include <qwgtlib/pdfviewer.hpp>

#include <QHBoxLayout>
#include <QMainWindow>
#include <QPushButton>
#include <QSplitter>
#include <halgorithm/qwidget_cptr.hpp>


class MainWindow : public QMainWindow
{
    Q_OBJECT

  public:
    MainWindow(QWidget* parent = nullptr);
    ~MainWindow() = default;


    spt::qwidget_cptr<wgtm::PdfViewer>     pdfviewer;
    spt::qwidget_cptr<wgt::FileTreeViewer> fileTree;
    spt::qwidget_cptr<QSplitter>           central;
};

#endif // MAINWINDOW_HPP
