#ifndef RSTEDITOR_HPP
#define RSTEDITOR_HPP

#include "../textedit.hpp"
#include <QWidget>

namespace wgtm {
class RSTEditor
    : extends QWidget
    , implements TextEdit
{
    Q_OBJECT
  public:
    explicit RSTEditor(QWidget* parent = nullptr);

  signals:

  public slots:
};
} // namespace wgtm

#endif // RSTEDITOR_HPP
