#ifndef CODEEDITOR_HPP
#define CODEEDITOR_HPP


//===    Qt    ===//
#include <QFont>
#include <Qsci/qscilexer.h>


//===    STL   ===//
#include <memory>


//=== Sibling  ===//
#include "../../keyboard/keyboardsupport.hpp"
#include "qsciwrapper.hpp"


namespace wgt {
class CodeTextfield
    : public qsci::QSciWrapper
    , public KeyboardSupport
{
    Q_OBJECT
  public:
    CodeTextfield(QWidget* parent = nullptr);
    void setNewLexer(spt::LexerType type);


  protected:
    std::unique_ptr<QsciLexer> lexer;
    spt::LexerType             lexerType;
    QsciLexer*                 newLexer(spt::LexerType type) const;
    void                       keyPressEvent(QKeyEvent* e) override;
    QFont                      font;

  signals:
    void buildRequested();
    void saveRequested();

    // KeyboardSupport interface
  public:
    bool processKeyEvent(KeyEvent& event) override;
    void initShortcuts() override;
};
} // namespace wgt


#endif // CODEEDITOR_HPP
