#ifndef SMARTCONTEXTMENU_HPP
#define SMARTCONTEXTMENU_HPP

/*!
 * \file smartcontextmenu.hpp
 * \brief
 */

//===    Qt    ===//
#include <QAction>
#include <QApplication>
#include <QDialog>
#include <QFile>
#include <QHBoxLayout>
#include <QMenu>
#include <QMouseEvent>
#include <QPoint>
#include <QVariant>
#include <QVector>
#include <QWidget>

//===    STL   ===//
#include <memory>
#include <vector>

//=== Internal ===//
#include <hdebugmacro/all.hpp>
#include <hsupport/qjson.hpp>
#include <hsupport/misc.hpp>

//=== Sibling  ===//
#include "menuactionresult.hpp"
#include "scm_debug.hpp"
#include "smartcontextmenu_scene.hpp"


//  ////////////////////////////////////

namespace wgt {
class SmartContextMenu : public QDialog
{
    Q_OBJECT

  public:
    explicit SmartContextMenu(
        QString  layoutFilePath,
        QString  name,
        QWidget* parent = nullptr);
    void exec(QPoint pos);
    void setPosChange(const QPoint& value);

  public slots:
    void emptyScenePressed(QPoint globalPos, QMouseEvent* event);

  private:
    QMenu        regularMenu;
    int          exec() override;
    QHBoxLayout* mainLayout;
    QPoint       posChange;
    QPoint       startPos;


    SmartContextMenu_Scene* scene;

  private slots:
    void fromFile(QString file);
    void actionChosen(MenuActionResult args);

  signals:
    void smartMenuOptionChosen(MenuActionResult);
    void emptySpacePressed(QPoint globalPos, QMouseEvent* event);
};
} // namespace wgt

#endif // CONTEXTMENUTEST_HPP
