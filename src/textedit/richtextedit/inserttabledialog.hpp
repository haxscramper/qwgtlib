#ifndef INSERTTABLEDIALOG_HPP
#define INSERTTABLEDIALOG_HPP

#include <QDialog>
#include <QGridLayout>
#include <QLabel>
#include <QPushButton>
#include <QSpinBox>
#include <QTextTableFormat>

namespace wgt {
class InsertTableDialog : public QDialog
{
    Q_OBJECT
  public:
    explicit InsertTableDialog(QWidget* parent = nullptr);

  private:
    QSpinBox*    rowCount_spbx;
    QSpinBox*    columnCount_spbx;
    QLabel*      rowCount_label;
    QLabel*      columnCount_label;
    QPushButton* accept_pbtn;
    QPushButton* cancel_pbtn;
    QGridLayout* layout;

  signals:
    void tableSettingsChoosen(
        int              rows,
        int              columns,
        QTextTableFormat tableFormat);

  public slots:
};
} // namespace wgt

#endif // INSERTTABLEDIALOG_HPP
