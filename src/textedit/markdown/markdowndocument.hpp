#ifndef DOCUMENT_H
#define DOCUMENT_H

#include <QObject>
#include <QString>

class MarkdownDocument : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString text MEMBER text NOTIFY textChanged FINAL)
  public:
    explicit MarkdownDocument(QObject* parent = nullptr)
        : QObject(parent) {
    }
    void setText(const QString& textNew);

  signals:
    void textChanged(const QString& text);

  private:
    QString text = "";
};

#endif // DOCUMENT_H
