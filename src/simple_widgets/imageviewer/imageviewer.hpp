#ifndef IMAGEVIEWER_HPP
#define IMAGEVIEWER_HPP

#include "../../base/widgetmanageable.hpp"
#include <QLabel>
#include <QPixmap>
#include <QVBoxLayout>
#include <QWidget>
#include <halgorithm/qwidget_cptr.hpp>

namespace wgtm {
/*!
 * Widget to display (not edit) images
 */
class ImageViewer
    : extends QWidget
    , implements wgtm::WidgetManageable
{
    Q_OBJECT
  public:
    explicit ImageViewer(QWidget* parent = nullptr);

  private:
    QPixmap                        image;
    spt::qwidget_cptr<QVBoxLayout> layout;
    spt::qwidget_cptr<QLabel>      preview;


    // WidgetManageable interface
  public slots:
    void openFile(QString path) override;

    // QWidget interface
  protected:
    void resizeEvent(QResizeEvent* event) override;
};
} // namespace wgtm


#endif // IMAGEVIEWER_HPP
