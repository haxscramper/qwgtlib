#ifndef LODSELECTOR_HPP
#define LODSELECTOR_HPP

#include "lodsupport.hpp"
#include <QComboBox>
#include <QFrame>
#include <QHBoxLayout>

class LODSelector : public QFrame
{
    Q_OBJECT
  public:
    explicit LODSelector(QWidget* parent = nullptr);

  signals:
    void LODChanged(LODSupport::LODLevel newLOD);
    void UIDensityChanged(LODSupport::UIDensity newUIDensity);

  private:
    void         initUI();
    void         initSignals();
    QComboBox*   LOD_cbox;
    QComboBox*   UIDensity_cbox;
    QHBoxLayout* mainLayout;
};

#endif // LODSELECTOR_HPP
