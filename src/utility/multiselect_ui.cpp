#include "multiselect.hpp"

namespace wgt {
void MultiSelect::initUI() {
    {
        lineEdit     = new QLineEdit(this);
        selector     = new QListView(this);
        selected     = new QListView(this);
        label        = new QLabel(this);
        addTagBtn    = new QPushButton(this);
        cancelTagBtn = new QToolButton(this);

        addTagBtn->setText("Add tag");
        label->setAlignment(Qt::AlignCenter);
        selector->setModel(&tagsPoolModel);
        cancelTagBtn->setIcon(
            qApp->style()->standardIcon(QStyle::SP_DialogCancelButton));
    }
    {
        mainLayout = new QGridLayout(this);
        mainLayout->addWidget(label, 1, 1, 1, 3);
        mainLayout->addWidget(selected, 2, 1, 1, 3);
        mainLayout->addWidget(cancelTagBtn, 3, 1, 1, 1);
        mainLayout->addWidget(lineEdit, 3, 2, 1, 1);
        mainLayout->addWidget(addTagBtn, 3, 3, 1, 1);
        mainLayout->addWidget(selector, 4, 1, 1, 3);
        this->setLayout(mainLayout);
    }

    {
        this->setContentsMargins(spt::equalMargins(2));
        mainLayout->setContentsMargins(spt::equalMargins(2));
    }

    // Todo add ability to edit tags from view (do not orget to update main
    // tags list)
    selected->setEditTriggers(QAbstractItemView::NoEditTriggers);
    selector->setEditTriggers(QAbstractItemView::NoEditTriggers);
}

void MultiSelect::initContent() {
}

void MultiSelect::setupSignals() {
    connect(selector, &QListView::doubleClicked, [&](QModelIndex index) {
        QString selectedItem = qvariant_cast<QString>(index.data());
        this->addTag(selectedItem);
    });

    connect(selected, &QListView::doubleClicked, [&](QModelIndex index) {
        QString selectedItem = qvariant_cast<QString>(index.data());
        selectedList.removeTag(selectedItem);
        this->updateUI();
    });

    connect(addTagBtn, &QPushButton::pressed, [&]() {
        this->addTag(lineEdit->text());
    });

    connect(lineEdit, &QLineEdit::returnPressed, [&]() {
        this->addTag(lineEdit->text());
    });

    connect(
        cancelTagBtn, &QToolButton::clicked, [&]() { lineEdit->clear(); });
}
} // namespace wgt
