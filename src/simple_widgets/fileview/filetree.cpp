#include "filetree.hpp"
#include "../../utility/stringprompt.hpp"

namespace wgt {
FileTree::FileTree(QWidget* parent)
    : QTreeView(parent)
    , fileSystemModel(new QFileSystemModel)
    , fileSystemFilter(new FileTreeFilter) {
    initUI();
    initSignals();
    initShortcuts();

    fileSystemModel->setReadOnly(false);
    this->setEditTriggers(QAbstractItemView::NoEditTriggers);
    this->setContextMenuPolicy(Qt::CustomContextMenu);
}

void FileTree::initUI() {
    fileSystemModel->setRootPath(QDir::homePath());
    fileSystemModel->setFilter(QDir::NoDot | QDir::AllDirs | QDir::Files);

    fileSystemFilter->setDynamicSortFilter(true);
    fileSystemFilter->setSourceModel(fileSystemModel);
    fileSystemFilter->setFilterKeyColumn(0);
    fileSystemFilter->setRecursiveFilteringEnabled(true);

    this->setModel(fileSystemFilter);
    QModelIndex rootIndex = fileSystemFilter->mapFromSource(
        fileSystemModel->index(QDir::homePath()));
    this->setRootIndex(rootIndex);

    for (int i = 1; i < fileSystemModel->columnCount(); i++) {
        this->hideColumn(i);
    }

    this->setSelectionMode(QAbstractItemView::ExtendedSelection);
    this->setDragEnabled(true);
    this->setDropIndicatorShown(true);
    this->viewport()->setAcceptDrops(true);

    this->setDragDropMode(QAbstractItemView::InternalMove);
}


void FileTree::showContextMenu(const QPoint& pos) {
    SmartContextMenu contextMenu(
        contextMenuSettings.absoluteFilePath(), "Actions", this);

    connect(
        &contextMenu,
        &SmartContextMenu::smartMenuOptionChosen,
        this,
        &FileTree::smartContextMenuActionChosen);

    connect(
        &contextMenu,
        &SmartContextMenu::emptySpacePressed,
        this,
        &FileTree::contextMenuEmptySpacePressed);

    contextMenu.exec(mapToGlobal(pos));
}


void FileTree::smartContextMenuActionChosen(MenuActionResult result) {
    QModelIndex index    = indexAt(mapFromGlobal(result.startPos));
    QString     filePath = getFileFromIndex(index);

    if (result.name == "copy-path") {
        QApplication::clipboard()->setText(filePath);
    } else if (result.name == "delete-selected-file") {
        if (selectedIndexes().isEmpty()) {
            QFileInfo fileInfo(filePath);
            if (fileInfo.isFile()) {
                QFile(filePath).remove();
            } else if (fileInfo.isDir()) {
                QDir().rmdir(filePath);
            }
        } else {
            for (QModelIndex index : selectedIndexes()) {
                QString   file = getFileFromIndex(index);
                QFileInfo fileInfo(file);
                if (fileInfo.isFile()) {
                    QFile(file).remove();
                } else if (fileInfo.isDir()) {
                    QDir().rmdir(file);
                }
            }
        }
    } else if (result.name == "duplicate-selected-file") {
        QFile file(filePath);
        file.copy(getDuplicateFileName(filePath));
    } else if (result.name == "copy-dir-path") {
        QApplication::clipboard()->setText(
            QFileInfo(filePath).dir().path());
    } else if (result.name == "rename-file") {
        renameFile(index);
    } else if (result.name == "create-new-folder") {
        QString newDirPath = rootDirectory.absolutePath() + "/new_folder";
        newDirPath         = getDuplicateFileName(newDirPath, "");
        LOG << "New folder:" << newDirPath;
        QDir().mkpath(newDirPath);
    }
}


void FileTree::contextMenuEmptySpacePressed(
    QPoint       pressGlobalPos,
    QMouseEvent* event) {
    pressGlobalPos -= QPoint(
        0, rowHeight(indexAt(mapFromGlobal(pressGlobalPos))));
    event->setLocalPos(mapFromGlobal(pressGlobalPos));
    //#    setCurrentIndex(indexAt(mapFromGlobal(pressGlobalPos)));
    FileTree::mousePressEvent(event);
}


void FileTree::initSignals() {
    connect(this, &FileTree::doubleClicked, this, &FileTree::openIndex);
    connect(this, &FileTree::clicked, this, &FileTree::previewIndex);
    connect(
        this,
        &FileTree::customContextMenuRequested,
        this,
        &FileTree::showContextMenu);

    connect(
        fileSystemModel,
        &QFileSystemModel::fileRenamed,
        [&](const QString& dir,
            const QString& oldPath,
            const QString& newPath) {
            Q_UNUSED(dir);
            emit fileRenamed(oldPath, newPath);
        });
}

void FileTree::initShortcuts() {
    shortcuts.addBinding("Backspace", "directory-up");
    keyboardActions["directory-up"] =
        [&]([[maybe_unused]] wgt::KBDParameters& internalParameters,
            [[maybe_unused]] json&               userParameters) -> bool {
        this->dirUp();
        return true;
    };

    shortcuts.addBinding("F2", "rename-selected-entry");
    keyboardActions["rename-selected-entry"] =
        [&]([[maybe_unused]] wgt::KBDParameters& internalParameters,
            [[maybe_unused]] json&               userParameters) -> bool {
        QModelIndexList selected = this->selectedIndexes();
        if (selected.size() < 1) {
            return false;
        } else {
            // Need to create pointer to model index that will not go out
            // of scope when function execution is finished.
            QModelIndex* index = new QModelIndex();
            *index             = selected.back();

            wgt::StringPrompt prompt;

            QRect  rect = visualRect(*index);
            QPoint pos  = mapToGlobal(rect.topLeft());


            connect(
                &prompt,
                &wgt::StringPrompt::stringSelected,
                [&](QString str) {
                    fileSystemModel->setData(
                        fileSystemFilter->mapToSource(*index), str);
                    delete index; // Now, deleting pointer to index
                });

            QString fileName = qvariant_cast<QString>(index->data());
            prompt.setText(fileName);
            prompt.selectAllUntil(".");

            prompt.grabFocus();

            prompt.move(pos);
            prompt.exec();

            /// \todo Use custom line editor
            return true;
        }
    };

    shortcuts.addBinding("return", "select-current-entry");
    keyboardActions["select-current-entry"] =
        [&]([[maybe_unused]] wgt::KBDParameters& internalParameters,
            [[maybe_unused]] json&               userParameters) -> bool {
        QModelIndexList selected = this->selectedIndexes();
        if (selected.size() < 1) {
            return false;
        } else {
            QModelIndex index = selected.back();
            openIndex(index);
            return true;
        }
    };
}

void FileTree::keyPressEvent(QKeyEvent* event) {
    wgt::KeyEvent keyEvent(event);
    processKeyEvent(keyEvent);
    QTreeView::keyPressEvent(event);

    DEBUG_FINALIZE_WINDOW
}

bool FileTree::processKeyEvent(KeyEvent& event) {
    QString            command = shortcuts.getCommand(event.getSequence());
    wgt::KBDParameters parameters;
    return executeCommand(command, parameters);
}

void FileTree::renameFile(QModelIndex fileIndex) {
    edit(fileIndex);
}


void FileTree::mousePressEvent(QMouseEvent* event) {
    QModelIndex index   = indexAt(event->pos());
    qint64      elapsed = clickTimer.restart();
    if (index == currentIndex() && elapsed > clickedTwiceInterval.first
        && elapsed < clickedTwiceInterval.second
        && event->button() == Qt::LeftButton) {
        edit(index);
    }
    QTreeView::mousePressEvent(event);
}
} // namespace wgt
