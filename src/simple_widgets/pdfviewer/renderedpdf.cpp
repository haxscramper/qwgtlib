#include "renderedpdf.hpp"
#include <halgorithm/all.hpp>
#include <hdebugmacro/all.hpp>
#include <hsupport/misc.hpp>


void RenderedPDF::openFile(QString path) {
    if (QFile::exists(path)) {
        document.reset(Poppler::Document::load(path));
        if (document.get() != nullptr) {
            LOG << "Document with" << document->numPages() << "pages";

            document->setRenderHint(Poppler::Document::Antialiasing);
            document->setRenderHint(Poppler::Document::TextAntialiasing);


            rendered.clear();
            rendered.reserve(document->numPages());
        } else {
            ERROR << "Could not open document";
        }
    }
}

QImage RenderedPDF::getPage(uint pageIdx, qreal dpiX, qreal dpiY) {
    if (rendered.find(pageIdx) != rendered.end()) {
        return rendered.at(pageIdx);
    } else {
        if (pageIdx < document->numPages()) {
            rendered[pageIdx] = document->page(pageIdx)->renderToImage(
                dpiX, dpiY);
            return rendered.at(pageIdx);
        } else {
            throw std::invalid_argument("Page index is out of range");
        }
    }
}

uint RenderedPDF::getPageCount() {
    return document->numPages();
}
