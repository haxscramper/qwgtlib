#include "codeeditor.hpp"

namespace wgtm {
void CodeEditor::openFile(QString path) {
    QFile file(path);
    if (file.exists()) {
        file.open(QIODevice::ReadWrite);
        setText(file.readAll());
        currentFile = path;
    }
    file.close();
}

void CodeEditor::writeFile(QFile& file) {
    bool succes = file.open(QIODevice::ReadWrite);
    assert(succes);
    if (succes) {
        QTextStream out(&file);
        out << this->getText();
        file.flush();
        file.close();
    }
}
} // namespace wgtm
