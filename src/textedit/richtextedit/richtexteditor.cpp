#include "richtexteditor.hpp"

// TODO set default font
namespace wgtm {
RichTextEditor::RichTextEditor(QString _iconDir, QWidget* parent)
    : RTE_Widget(_iconDir, parent) {
    m_lastBlockList = nullptr;
    textfield->setTabStopDistance(40);

    fontsize_h1 = 18;
    fontsize_h2 = 16;
    fontsize_h3 = 14;
    fontsize_h4 = 12;

    fontChanged(textfield->font());
    bgColorChanged(textfield->textColor());


    initMenuButtons();


    // font size

    QFontDatabase db;
    for (int size : db.standardSizes()) {
        fontSize_cbox->addItem(QString::number(size));
    }


    fontSize_cbox->setCurrentIndex(fontSize_cbox->findText(
        QString::number(QApplication::font().pointSize())));

    // text foreground color

    QPixmap pix(16, 16);
    pix.fill(QApplication::palette().foreground().color());
    foregColor_tbtn->setIcon(pix);


    // text background color

    pix.fill(QApplication::palette().background().color());
    backgColor_tbtn->setIcon(pix);

    initSignals();
}
} // namespace wgtm
