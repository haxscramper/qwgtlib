#ifndef FILETREEFILTER_H
#define FILETREEFILTER_H

#include <QDebug>
#include <QSortFilterProxyModel>

#include <set>
#include <string>
#include <hsupport/misc.hpp>
#include <vector>


namespace wgt {
class FileTreeFilter : public QSortFilterProxyModel
{
    Q_OBJECT

  public:
    void addBannedExtensions(std::vector<QString> extensions);

    bool isInvertedBanned() const;
    void setInvertBanned(bool value);

  protected:
    bool filterAcceptsRow(int sourceRow, const QModelIndex& sourceParent)
        const override;

  private:
    std::set<QString> bannedFiles;
    bool              invertBanned;
};
} // namespace wgt

#endif // FILETREEFILTER_H
