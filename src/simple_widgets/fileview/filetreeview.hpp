#ifndef FILETREEVIEWER_H
#define FILETREEVIEWER_H


/*!
 * \file filetreeview.hpp
 * \brief
 */

//===    Qt    ===//
#include <QComboBox>
#include <QHBoxLayout>
#include <QLineEdit>
#include <QPoint>
#include <QPushButton>
#include <QToolButton>
#include <QVBoxLayout>
#include <QWidget>


//=== Internal ===//
#include "../../simple_widgets/toolbar/toolbarsimple.hpp"
#include <halgorithm/qwidget_cptr.hpp>
#include <hdebugmacro/all.hpp>


//=== Sibling  ===//
#include "filetree.hpp"


//  ////////////////////////////////////


namespace wgt {
/*!
 * File tree widget with up button, line edit with current directory name
 * etc.
 */
class FileTreeViewer : public QWidget
{
    Q_OBJECT
  public:
    explicit FileTreeViewer(QWidget* parent = nullptr);
    virtual ~FileTreeViewer() = default;
    QString getRoot() const;

    //#=== File filtering
    void addBannedExtensions(std::vector<QString> extensions);
    bool isInvertedBanned() const;
    void setInvertBanned(bool value);

  public slots:
    void setRoot(QString rootDirectory);

  signals:
    void fileSelected(QString filePath);
    void directorySelected(QString directoryPath);
    void rootChanged(QString directoryPath);
    void fileRenamed(QString oldName, QString newName);


  private:
    spt::qwidget_cptr<FileTree>      fileTree;
    spt::qwidget_cptr<QVBoxLayout>   mainLayout;
    spt::qwidget_cptr<QPushButton>   dirUpButton;
    spt::qwidget_cptr<QLineEdit>     currentPathEdit;
    spt::qwidget_cptr<QAction>       deleteFileAct;
    spt::qwidget_cptr<ToolbarSimple> toolbar;


    void initUI();
    void initSignals();
};
} // namespace wgt

#endif // FILETREEVIEWER_H
