#ifndef WGTM_DEBUG_MACRO_HPP
#define WGTM_DEBUG_MACRO_HPP

#include <hdebugmacro/all.hpp>


#ifdef WGTMWIDGET_DEBUG
#    define WGTM_LOG DebugLogger::log()
#    define WGTM_FUNC_BEGIN DEBUG_FUNCTION_BEGIN
#    define WGTM_FUNC_END DEBUG_FUNCTION_END
#    define WGTM_FUNC_RET(message) DEBUG_FUNCTION_PRE_RETURN(message)
#else
#    define WGTM_LOG VOID_LOG
#    define WGTM_FUNC_BEGIN
#    define WGTM_FUNC_END
#    define WGTM_FUNC_RET(message)
#endif

#endif // WGTM_DEBUG_MACRO_HPP
