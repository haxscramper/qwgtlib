#ifndef WEBVIEW_H
#define WEBVIEW_H

#include "../../base/widgetmanageable.hpp"

#include <QObject>
#include <QWebEngineView>
#include <QWidget>

namespace wgtm {
/*!
 * \brief Simple preview for HTML files
 */
class WebView
    : extends QWebEngineView
    , implements WidgetManageable
{
  public:
    WebView(QWidget* parent = nullptr);

    // WidgetManageable interface
  public slots:
    void    openFile(QString path) override;
    void    saveFile() override;
    QString getWidgetName() const override;
};
} // namespace wgtm

#endif // WEBVIEW_H
