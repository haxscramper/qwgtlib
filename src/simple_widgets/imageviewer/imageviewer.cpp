#include "imageviewer.hpp"

namespace wgtm {
ImageViewer::ImageViewer(QWidget* parent)
    : QWidget(parent)
    , layout(new QVBoxLayout(this))
    , preview(new QLabel(this))
//#  ,
{
    setLayout(layout);
    layout->addWidget(preview);
    preview->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);
}

void ImageViewer::openFile(QString path) {
    if (QFile::exists(path)) {
        image = QPixmap(path);
        preview->setPixmap(image);
    }
}

void ImageViewer::resizeEvent(QResizeEvent* event) {
    preview->setPixmap(image.scaled(
        event->size().width(),
        event->size().height(),
        Qt::KeepAspectRatio));

    QWidget::resizeEvent(event);
}
} // namespace wgtm
