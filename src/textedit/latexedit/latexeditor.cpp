#include "../codeeditor.hpp"

#include "latexbuildpreview.hpp"
#include "latexeditor.hpp"
#include "latexquickpreview.hpp"

#include <toolbarsimple.hpp>
#include <waitingspinner.h>


namespace wgtm {
//#  /////////////////////////////////////////////////////////////////////
//#  Special member funcitons
//#  /////////////////////////////////////////////////////////////////////


LatexEditor::LatexEditor(QWidget* parent)
    : QWidget(parent)
    , buildPreview(new wgt::LatexBuildPreview(this))
    , quickPreview(new wgt::LatexQuickPreview(this))
    , layout(new QHBoxLayout(this))
    , splitter(new QSplitter(this))
    , codeEditor(new wgtm::CodeEditor(this))
    , resultLayout(new QVBoxLayout(this))
    , previewSwitch(new QPushButton(this))
    , previewFrame(new QWidget(this))
    , resultPreview(new QStackedWidget(this))
    , previewToolbar(new ToolbarSimple(this))
    , buildWait(new wgt::WaitingSpinner(this))
//  ,
{
    initUI();
    initSignals();

    codeEditor->setLexer(spt::LexerType::Tex);
    codeEditor->setText(R"tex($
\sum\limits_{k=0}^n C_n^k = 2^n\\
\sum\limits_{k=1}^n (-1)^{k-1} C_n^k = 1\\
\sum\limits_{k=0}^n (C_n^k)^2 = C_{2n}^n\\
\sum\limits_{p=0}^k C_{n+p}^m = C_{n+k+1}^{m+1} - C_n^{m+1}\\
\sum\limits_{k=0}^{m-1} C_{n+k}^n=C_{n+m}^{n+1}\\
\sum\limits_{k=0}^n (-1)^k \cdot C_n^k = 0\\
\sum\limits_{k=0}^n C_n^k x^k\\
\sum\limits_{n=k}^m \frac{1}{n} C_n^k = \frac{1}{n}C_n^k\\
C_n^k = \frac{n!}{k!(n-k)!}\\
C_n^0 = C_n^n =1\\
C_n^1=C_n^n-1=n\\
C_n^k=C_n^{n-k}\\
C_{n+1}^k=C_n^{k-1}+C_n^k\\
C_n^k=C_{n-1}^{k-1}+C_{n-1}^k\\
(a+b)^n=\sum\limits_{k=0}^n C_n^k\cdot a^{n-k}b^k\\
S_n = \frac{2a_1+d(n-1)}{2}\cdot n\\
S_n = \frac{b_1(q_n-1)}{q-1},q\neq 1
$)tex");

    splitter->setSizes({350, 350});
}


//#  /////////////////////////////////////////////////////////////////////
//#  Initialization functions
//#  /////////////////////////////////////////////////////////////////////


void LatexEditor::initUI() {
    //#    setLayout(layout);
    layout->addWidget(splitter);
    splitter->setChildrenCollapsible(false);
    splitter->addWidget(codeEditor);


    previewFrame->setLayout(resultLayout);
    resultLayout->addWidget(previewToolbar);

    previewToolbar->addWidget(previewSwitch);
    int buttonSize = 24;
    buildWait->setLineLength(buttonSize);
    buildWait->setNumberOfLines(12);

    buildWait->fitInto({buttonSize, buttonSize});

    previewToolbar->addWidget(
        buildWait, {buttonSize, buttonSize}, {buttonSize, buttonSize});

    resultLayout->addWidget(resultPreview);
    previewFrame->setContentsMargins(spt::zeroMargins());
    resultLayout->setContentsMargins(spt::zeroMargins());

    splitter->addWidget(previewFrame);

    resultPreview->addWidget(quickPreview);
    resultPreview->addWidget(buildPreview);

    setBuildPreview();
}

void LatexEditor::initSignals() {
    connect(codeEditor.get(), &wgtm::CodeEditor::buildRequested, [&]() {
        getTexEditor()->setSource(codeEditor->getText());
        getTexEditor()->build();
        getTexEditor()->updateUI();
    });

    connect(previewSwitch, &QPushButton::pressed, [&]() {
        if (isInBuildMode) {
            setQuickPreview();
        } else {
            setBuildPreview();
        }
    });

    connect(buildPreview, &wgt::LatexBuildPreview::buildStarted, [&]() {
        buildWait->start();
    });

    connect(buildPreview, &wgt::LatexBuildPreview::buildFinished, [&]() {
        buildWait->stop();
    });
}

void LatexEditor::setText(QString text) {
    codeEditor->setText(text);
}


//#  /////////////////////////////////////////////////////////////////////
//#  Member access functions
//#  /////////////////////////////////////////////////////////////////////


void LatexEditor::setQuickPreview() {
    resultPreview->setCurrentIndex(0);
    previewSwitch->setText("Quick preview");
    isInBuildMode = false;
}

void LatexEditor::setBuildPreview() {
    resultPreview->setCurrentIndex(1);
    previewSwitch->setText("Build preview");
    isInBuildMode = true;
}

wgt::LatexPreview* LatexEditor::getTexEditor() {
    return isInBuildMode ? buildPreview.cast_to<wgt::LatexPreview>()
                         : quickPreview.cast_to<wgt::LatexPreview>();
}
} // namespace wgtm
