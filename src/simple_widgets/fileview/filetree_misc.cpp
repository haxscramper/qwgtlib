#include "filetree.hpp"

namespace wgt {
QString FileTree::getFileFromIndex(const QModelIndex& index) {
    QFileInfo fileInfo = fileSystemModel->fileInfo(
        fileSystemFilter->mapToSource(index));
    return fileInfo.absoluteFilePath();
}

void FileTree::addBannedExtensions(std::vector<QString> files) {
    fileSystemFilter->addBannedExtensions(std::move(files));
}

bool FileTree::isInvertedBanned() const {
    return fileSystemFilter->isInvertedBanned();
}

void FileTree::setInvertBanned(bool value) {
    fileSystemFilter->setInvertBanned(value);
}

void FileTree::openIndex(const QModelIndex& index) {
    QFileInfo fileInfo = fileSystemModel->fileInfo(
        fileSystemFilter->mapToSource(index));
    if (fileInfo.isDir()) {
        this->setRoot(fileInfo.absoluteFilePath());
    } else if (fileInfo.isFile()) {
        emit fileSelected(fileInfo.absoluteFilePath());
    }
}

void FileTree::previewIndex(const QModelIndex& index) {
    QFileInfo fileInfo = fileSystemModel->fileInfo(
        fileSystemFilter->mapToSource(index));
    if (fileInfo.isDir()) {
        emit directorySelected(fileInfo.absoluteFilePath());
    }
}

void FileTree::setRoot(QDir newRoot) {
    if (newRoot.exists()) {
        this->rootDirectory = newRoot;
    }
}

void FileTree::setRoot(QString newRoot) {
    if (QDir(newRoot).exists()) {
        fileSystemModel->setRootPath(newRoot);
        QModelIndex rootIndex = fileSystemFilter->mapFromSource(
            fileSystemModel->index(newRoot));
        this->setRootIndex(rootIndex);
        this->rootDirectory.setPath(newRoot);
        emit rootChanged(newRoot);
    }
}

QString FileTree::getRoot() const {
    return rootDirectory.absolutePath();
}


void FileTree::dirUp() {
    QDir newRoot(this->rootDirectory.absolutePath());
    newRoot.cdUp();
    setRoot(newRoot.absolutePath());
}


QString FileTree::getDuplicateFileName(QString filePath, QString append) {
    QFileInfo file(filePath);
    QString   extension = file.isDir() ? "" : "." + file.suffix();
    QString   copyPath  = file.dir().path() + "/" + file.baseName()
                       + (append.isEmpty() ? "" : " " + append + " ");
    QString newPath = copyPath;
    uint    copyNum = 1;
    while (QFile(copyPath + extension).exists()
           && QFile(newPath + extension).exists()) {
        newPath = copyPath + "(" + QString::number(copyNum) + ")";
        copyNum++;
    }
    newPath += extension;

    return newPath;
}
} // namespace wgt
