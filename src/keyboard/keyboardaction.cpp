#include "keyboardaction.hpp"

namespace wgt {
KeyboardAction::KeyboardAction() {
    action = [](KBDParameters& internalParameter,
                json&          userParameters) -> bool {
        Q_UNUSED(internalParameter)
        Q_UNUSED(userParameters)
        return false;
    };
}

KeyboardAction::KeyboardAction(
    QString                                    _name,
    std::function<bool(KBDParameters&, json&)> _action) {
    name   = _name;
    action = _action;
}

bool KeyboardAction::operator()(
    KBDParameters& internalParameter,
    json&          userParameters) {
    return action(internalParameter, userParameters);
}

KeyboardAction& KeyboardAction::operator=(
    std::function<bool(KBDParameters&, json&)> _action) {
    action = _action;
    return *this;
}

QString KeyboardAction::getName() const {
    return name;
}

void KeyboardAction::setName(const QString& value) {
    name = value;
}
} // namespace wgt
