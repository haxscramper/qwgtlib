#ifndef RICHTEXTWIDGET_H
#define RICHTEXTWIDGET_H

#include "../../base/widgetmanageable.hpp"
#include "../textedit.hpp"
#include "rte_widget.hpp"

#include "../../base/lodsupport.hpp"
#include <QApplication>
#include <QBuffer>
#include <QByteArray>
#include <QClipboard>
#include <QColorDialog>
#include <QDialog>
#include <QFileDialog>
#include <QFontDatabase>
#include <QImageReader>
#include <QInputDialog>
#include <QMenu>
#include <QMimeData>
#include <QPlainTextEdit>
#include <QPointer>
#include <QSettings>
#include <QTextCodec>
#include <QTextList>
#include <QUrl>
#include <QtDebug>
#include <hdebugmacro/all.hpp>

namespace wgtm {
class RichTextEditor
    : extends RTE_Widget
    , implements TextEdit
{
    Q_OBJECT
  public:
    RichTextEditor(QString _iconDir, QWidget* parent = nullptr);

    QString        toPlainText() const;
    QString        toHtml() const;
    QTextDocument* document();
    QTextCursor    textCursor() const;
    void           setTextCursor(const QTextCursor& cursor);

  protected:
    void mergeFormatOnWordOrSelection(const QTextCharFormat& format);
    void fontChanged(const QFont& f);
    void fgColorChanged(const QColor& c);
    void bgColorChanged(const QColor& c);
    void list(bool checked, QTextListFormat::Style style);
    void indent(int delta);
    void focusInEvent(QFocusEvent* event) override;

    QStringList m_paragraphItems;
    int         fontsize_h1;
    int         fontsize_h2;
    int         fontsize_h3;
    int         fontsize_h4;

    enum ParagraphItems
    {
        ParagraphStandard = 0,
        ParagraphHeading1,
        ParagraphHeading2,
        ParagraphHeading3,
        ParagraphHeading4,
        ParagraphMonospace
    };

    QPointer<QTextList> m_lastBlockList;
    QString             defaultFontSize = "14";

  private:
    void initMenuButtons();

  public slots:
    void setPlainText(const QString& text);
    void setHtml(const QString& text);

  protected slots:
    void textRemoveFormat();
    void textRemoveAllFormat();
    void textBold();
    void textUnderline();
    void textStrikeout();
    void textItalic();
    void textSize(const QString& p);
    void textLink(bool checked);
    void textStyle(int index);
    void textFgColor();
    void textBgColor();
    void listBullet(bool checked);
    void listOrdered(bool checked);
    void slotCurrentCharFormatChanged(const QTextCharFormat& format);
    void slotCursorPositionChanged();
    void slotClipboardDataChanged();
    void increaseIndentation();
    void decreaseIndentation();
    void insertImage();
    void textSource();


    // WidgetInterfac interfaces
  protected:
    void initSignals() override;

    // WidgetManageable interface
  public:
    bool    isSupported(const QString& extension) override;
    QString getWidgetName() const override;

  public slots:
    void newFile() override;
    void writeFile(QString path) override;
    void openFile(QString path) override;

    // TextEdit interface
  public:
    void    setText(QString text) override;
    QString getText() override;
    void    clean() override;
};

} // namespace wgtm
#endif // RICHTEXTEDITOR_WIDGET_H
