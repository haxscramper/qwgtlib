#include "rte_widget.hpp"

#include <hsupport/icons.hpp>

using iconEnum = spt::icons::Standard_Action_Icons;

namespace wgtm {

void RTE_Widget::setupUI() {
    mainLayout = new QVBoxLayout;
    mainLayout->setSpacing(2);
    mainLayout->setContentsMargins(spt::equalMargins(2));

    toolbar = new ToolbarSimple(this);


    setupButtons();

    mainLayout->addWidget(toolbar);

    textfield = new RTE_Textfield;
    textfield->setAutoFormatting(QTextEdit::AutoNone);
    textfield->setTabChangesFocus(true);

    mainLayout->addWidget(textfield);
    this->setLayout(mainLayout);

    textfield->setMinimumSize(0, 0);
    toolbar->setMinimumSize(0, 0);
    this->setMinimumSize(0, 0);
}


void RTE_Widget::setupButtons() {
    // Paragraph checkbox
    {
        paragraph_cbox = new QComboBox(toolbar);
        paragraph_cbox->setFocusPolicy(Qt::ClickFocus);
        paragraph_cbox->setEditable(true);
        paragraph_cbox->setMinimumSize(128, buttonHeight);
        toolbar->addWidget(paragraph_cbox, LODLevel::Medium);
    }

    toolbar->addWidget(new wgt::UISeparator(this));

    // Undo button
    {
        undo_tbtn = new QToolButton(toolbar);
        undo_tbtn->setFocusPolicy(Qt::ClickFocus);
        undo_tbtn->setEnabled(false);
        undo_tbtn->setIcon(spt::toIcon(iconEnum::edit_undo));
        undo_tbtn->setIconSize(iconSize);
        undo_tbtn->setMinimumSize(buttonMinSize);
        toolbar->addWidget(undo_tbtn, LODLevel::All);
    }

    // Redo button
    {
        redo_tbtn = new QToolButton(toolbar);
        redo_tbtn->setFocusPolicy(Qt::ClickFocus);
        redo_tbtn->setIcon(QIcon::fromTheme("edit-redo"));
        redo_tbtn->setIconSize(iconSize);
        redo_tbtn->setMinimumSize(buttonMinSize);
        toolbar->addWidget(redo_tbtn, LODLevel::All);
    }

    // Cut button
    {
        cut_tbtn = new QToolButton(toolbar);
        cut_tbtn->setFocusPolicy(Qt::ClickFocus);
        cut_tbtn->setIcon(QIcon::fromTheme("edit-cut"));
        cut_tbtn->setIconSize(iconSize);
        cut_tbtn->setMinimumSize(buttonMinSize);
        toolbar->addWidget(cut_tbtn, LODLevel::All);
    }

    // Copy button
    {
        copy_tbtn = new QToolButton(toolbar);
        copy_tbtn->setFocusPolicy(Qt::ClickFocus);
        copy_tbtn->setIcon(QIcon::fromTheme("edit-copy"));
        copy_tbtn->setIconSize(iconSize);
        copy_tbtn->setMinimumSize(buttonMinSize);
        toolbar->addWidget(copy_tbtn, LODLevel::All);
    }

    // Paste button
    {
        paste_tbtn = new QToolButton(toolbar);
        paste_tbtn->setFocusPolicy(Qt::ClickFocus);
        paste_tbtn->setIcon(QIcon::fromTheme("edit-paste"));
        paste_tbtn->setIconSize(iconSize);
        paste_tbtn->setMinimumSize(buttonMinSize);
        toolbar->addWidget(paste_tbtn, LODLevel::All);
    }

    // Insert link button
    {
        link_tbtn = new QToolButton(toolbar);
        link_tbtn->setFocusPolicy(Qt::ClickFocus);
        link_tbtn->setIcon(QIcon::fromTheme("applications-internet"));
        link_tbtn->setIconSize(iconSize);
        link_tbtn->setMinimumSize(buttonMinSize);
        toolbar->addWidget(link_tbtn, LODLevel::Medium);
    }

    toolbar->addWidget(new wgt::UISeparator(this));

    QString iconThemeName = QStringLiteral("edit-undo");

    // Text-bold button
    {
        bold_tbtn = new QToolButton(toolbar);
        bold_tbtn->setFocusPolicy(Qt::ClickFocus);
        bold_tbtn->setIcon(
            QIcon(iconDir.absoluteFilePath("rte_icons/bold.svg")));
        bold_tbtn->setIconSize(iconSize);
        bold_tbtn->setCheckable(true);
        bold_tbtn->setMinimumSize(buttonMinSize);
        toolbar->addWidget(bold_tbtn, LODLevel::Minimal);
    }

    // Text-italic button
    {
        italic_tbtn = new QToolButton(toolbar);
        italic_tbtn->setFocusPolicy(Qt::ClickFocus);
        italic_tbtn->setIcon(
            QIcon(iconDir.absoluteFilePath("rte_icons/italic.svg")));
        italic_tbtn->setIconSize(iconSize);
        italic_tbtn->setCheckable(true);
        italic_tbtn->setMinimumSize(buttonMinSize);
        toolbar->addWidget(italic_tbtn, LODLevel::Minimal);
    }

    // Text-underline button
    {
        underline_tbtn = new QToolButton(toolbar);
        underline_tbtn->setFocusPolicy(Qt::ClickFocus);
        underline_tbtn->setIcon(
            QIcon(iconDir.absoluteFilePath("rte_icons/underline.svg")));
        underline_tbtn->setIconSize(iconSize);
        underline_tbtn->setCheckable(true);
        underline_tbtn->setMinimumSize(buttonMinSize);
        toolbar->addWidget(underline_tbtn, LODLevel::Minimal);
    }

    // Text-strikeout button
    {
        INFO << "Icon dir" << iconDir;
        LOG << "Loading icon from"
            << iconDir.absoluteFilePath("rte_icons/strikethrough.svg");

        strikeout_tbtn = new QToolButton(toolbar);
        strikeout_tbtn->setFocusPolicy(Qt::ClickFocus);
        strikeout_tbtn->setCheckable(true);
        strikeout_tbtn->setIcon(QIcon(
            iconDir.absoluteFilePath("rte_icons/strikethrough.svg")));
        strikeout_tbtn->setIconSize(iconSize);
        strikeout_tbtn->setMinimumSize(buttonMinSize);
        toolbar->addWidget(strikeout_tbtn, LODLevel::Minimal);
    }

    toolbar->addWidget(new wgt::UISeparator(this));

    // Bullet list button
    {
        bulletList_tbtn = new QToolButton(toolbar);
        bulletList_tbtn->setFocusPolicy(Qt::ClickFocus);
        bulletList_tbtn->setCheckable(true);
        bulletList_tbtn->setIcon(
            QIcon(iconDir.absoluteFilePath("rte_icons/bullet-list.svg")));
        bulletList_tbtn->setIconSize(iconSize);
        bulletList_tbtn->setMinimumSize(buttonMinSize);
        toolbar->addWidget(bulletList_tbtn, LODLevel::Minimal);
    }

    // Ordered list button
    {
        numberedList_tbtn = new QToolButton(toolbar);
        numberedList_tbtn->setFocusPolicy(Qt::ClickFocus);
        numberedList_tbtn->setCheckable(true);
        numberedList_tbtn->setIcon(QIcon(
            iconDir.absoluteFilePath("rte_icons/numbered-list.svg")));
        numberedList_tbtn->setIconSize(iconSize);
        numberedList_tbtn->setMinimumSize(buttonMinSize);
        toolbar->addWidget(numberedList_tbtn, LODLevel::Minimal);
    }

    // Insert horizontal line
    {
        insertHline_tbtn = new QToolButton(toolbar);
        insertHline_tbtn->setFocusPolicy(Qt::ClickFocus);
        insertHline_tbtn->setIcon(QIcon(
            iconDir.absoluteFilePath("rte_icons/horizontal-line.svg")));
        insertHline_tbtn->setIconSize(iconSize);
        insertHline_tbtn->setMinimumSize(buttonMinSize);
        toolbar->addWidget(insertHline_tbtn, LODLevel::Minimal);
    }

    // Insert table
    {
        insertTable_tbtn = new QToolButton(toolbar);
        insertTable_tbtn->setFocusPolicy(Qt::ClickFocus);
        insertTable_tbtn->setIcon(
            QIcon(iconDir.absoluteFilePath("rte_icons/insert-table.svg")));
        insertTable_tbtn->setIconSize(iconSize);
        insertTable_tbtn->setMinimumSize(buttonMinSize);
        toolbar->addWidget(insertTable_tbtn, LODLevel::Minimal);
    }

    // Indent more button
    {
        indentLess_tbtn = new QToolButton(toolbar);
        indentLess_tbtn->setFocusPolicy(Qt::ClickFocus);
        indentLess_tbtn->setIcon(QIcon(
            iconDir.absoluteFilePath("rte_icons/format-indent-less.svg")));
        indentLess_tbtn->setIconSize(iconSize);
        indentLess_tbtn->setMinimumSize(buttonMinSize);
        toolbar->addWidget(indentLess_tbtn, LODLevel::Minimal);
    }

    // Indent less button
    {
        indentMore_tbtn = new QToolButton(toolbar);
        indentMore_tbtn->setFocusPolicy(Qt::ClickFocus);
        indentMore_tbtn->setIcon(QIcon(
            iconDir.absoluteFilePath("rte_icons/format-indent-less.svg")));
        indentMore_tbtn->setIconSize(iconSize);
        indentMore_tbtn->setMinimumSize(buttonMinSize);
        toolbar->addWidget(indentMore_tbtn, LODLevel::Minimal);
    }

    toolbar->addWidget(new wgt::UISeparator(this));

    // Foreground color button
    {
        foregColor_tbtn = new QToolButton(toolbar);
        foregColor_tbtn->setMinimumSize(iconSize);
        foregColor_tbtn->setFocusPolicy(Qt::ClickFocus);
        foregColor_tbtn->setIconSize(iconSize);
        foregColor_tbtn->setMinimumSize(buttonMinSize);
        toolbar->addWidget(foregColor_tbtn, LODLevel::Minimal);
    }

    // Background color button
    {
        backgColor_tbtn = new QToolButton(toolbar);
        backgColor_tbtn->setMinimumSize(iconSize);
        backgColor_tbtn->setFocusPolicy(Qt::ClickFocus);
        backgColor_tbtn->setIconSize(iconSize);
        backgColor_tbtn->setMinimumSize(buttonMinSize);
        toolbar->addWidget(backgColor_tbtn, LODLevel::Minimal);
    }

    // Font size checkbox
    {
        fontSize_cbox = new QComboBox(toolbar);
        fontSize_cbox->setFocusPolicy(Qt::ClickFocus);
        fontSize_cbox->setEditable(true);
        fontSize_cbox->setSizePolicy(
            QSizePolicy::Minimum, QSizePolicy::Minimum);
        fontSize_cbox->setMinimumSize(48, buttonHeight);
        toolbar->addWidget(fontSize_cbox, LODLevel::Minimal);
    }

    toolbar->addWidget(new wgt::UISeparator(this));

    // Insert image button
    {
        insertImage_tbtn = new QToolButton(toolbar);
        insertImage_tbtn->setFocusPolicy(Qt::ClickFocus);
        insertImage_tbtn->setIcon(
            QIcon(iconDir.absoluteFilePath("rte_icons/insert-image.svg")));
        insertImage_tbtn->setIconSize(iconSize);
        insertImage_tbtn->setMinimumSize(buttonMinSize);
        toolbar->addWidget(insertImage_tbtn);
    }

    // Menu tool button
    {
        menu_tbtn = new QToolButton(toolbar);
        menu_tbtn->setFocusPolicy(Qt::ClickFocus);
        menu_tbtn->setMinimumSize(buttonMinSize);
        toolbar->addWidget(menu_tbtn);
    }
}

} // namespace wgtm
