/// \brief Abstract class for managing widgets inside workspace
#ifndef ABSTRACTBASE_H
#define ABSTRACTBASE_H


#include "../base/widgetinterface.hpp"
#include "../keyboard/keyboardfilter.hpp"
#include "../keyboard/keyboardsupport.hpp"
#include "../keyboard/keyseqence.hpp"
#include "wgtm_debug_macro.hpp"

#include <QDebug>
#include <QFileDialog>
#include <QFileInfo>
#include <QWidget>
#include <hsupport/misc.hpp>


class Workspace;

/// \brief Widgets that are not derived from widget manageable
namespace wgt {}

/// \brief Widget manageable and simple editor widgets
namespace wgtm {
/*!
 * \brief Base class for all widgets that can read/edit/save/create new
 * files
 */
class WidgetManageable
{
  public:
    WidgetManageable();
    virtual ~WidgetManageable();
    virtual QString getFile();

    virtual bool isSupported(const QString& extension);
    virtual void addSupported(const QString& extension);

    virtual bool isAllowed(const QString fileType);
    virtual void addAllowed(const QString fileType);

    virtual bool    isEmpty() const;
    virtual void    debugName();
    virtual QString getWidgetName() const;
    virtual QString getFileName();

    Workspace* getParentWorkspace() const;
    void       setParentWorkspace(Workspace* value);

  protected:
    QString          currentFile         = "";
    QVector<QString> supportedExtensions = {""};
    QVector<QString> allowedFileTypes    = {""};
    virtual void     overrideFile(QString path);
    virtual void     writeFile(QFile& file);
    virtual void     writeFile(QString path);
    virtual void     readFile(QFile& file);
    virtual void     updateContent();
    virtual void     updateUI();
    virtual bool     requiresSaving() const;
    virtual QString  getExtension() const;
    bool             isModified = false;
    Workspace*       parentWorkspace;

  public slots:
    virtual void    init();
    virtual void    saveFile();
    virtual void    newFile();
    virtual void    newFile(QString path);
    virtual void    saveFileAs();
    virtual void    openFile(QString path);
    virtual QString askPath(
        QString     folder             = "",
        QStringList acceptedExtensions = {""});
    virtual void saveTestFile(QString extension);
};

} // namespace wgtm

#endif // ABSTRACTBASE_H
