#include "filetreeview.hpp"

namespace wgt {
FileTreeViewer::FileTreeViewer(QWidget* parent)
    : QWidget(parent)
    , fileTree(new FileTree(this))
    , mainLayout(new QVBoxLayout())
    , dirUpButton(new QPushButton(this))
    , currentPathEdit(new QLineEdit(this))
    , deleteFileAct(new QAction(this))
    , toolbar(new ToolbarSimple(this))
//  ,
{
    this->initUI();
    this->initSignals();
}

QString FileTreeViewer::getRoot() const {
    return fileTree->getRoot();
}


void FileTreeViewer::addBannedExtensions(std::vector<QString> extensions) {
    fileTree->addBannedExtensions(std::move(extensions));
}


bool FileTreeViewer::isInvertedBanned() const {
    return fileTree->isInvertedBanned();
}


void FileTreeViewer::setInvertBanned(bool value) {
    fileTree->setInvertBanned(value);
}


void FileTreeViewer::initUI() {
    this->setLayout(mainLayout);

    toolbar->addWidget(dirUpButton, LODSupport::LODLevel::Zero);
    toolbar->setMinimumHeight(32);
    dirUpButton->setMinimumSize(96, 24);

    dirUpButton->setText("Up");

    fileTree->setRoot(QDir::currentPath());

    mainLayout->addWidget(toolbar);
    mainLayout->addWidget(fileTree);
    mainLayout->addWidget(currentPathEdit);

    this->setMinimumSize(0, 0);

    setContentsMargins(spt::equalMargins(2));
    mainLayout->setContentsMargins(spt::equalMargins(2));
}


void FileTreeViewer::initSignals() {
    connect(
        dirUpButton, &QPushButton::clicked, fileTree, &FileTree::dirUp);

    connect(
        fileTree,
        &FileTree::fileSelected,
        this,
        &FileTreeViewer::fileSelected);

    connect(
        fileTree,
        &FileTree::directorySelected,
        this,
        &FileTreeViewer::directorySelected);

    connect(
        fileTree,
        &FileTree::rootChanged,
        this,
        &FileTreeViewer::rootChanged);

    connect(
        this,
        &FileTreeViewer::rootChanged,
        currentPathEdit,
        &QLineEdit::setText);

    connect(
        currentPathEdit,
        &QLineEdit::textEdited,
        this,
        &FileTreeViewer::setRoot);

    connect(
        fileTree,
        &FileTree::fileRenamed,
        this,
        &FileTreeViewer::fileRenamed);
}

void FileTreeViewer::setRoot(QString rootDirectory) {
    fileTree->setRoot(rootDirectory);
}
} // namespace wgt
