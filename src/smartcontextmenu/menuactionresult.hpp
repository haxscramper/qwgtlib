#ifndef MENUACTIONRESULT_HPP
#define MENUACTIONRESULT_HPP

#include <QPoint>
#include <QVariant>
#include <hsupport/qjson.hpp>

namespace wgt {
struct MenuActionResult {
    QString name;
    json    args;
    QPoint  startPos;
};
} // namespace wgt

Q_DECLARE_METATYPE(wgt::MenuActionResult)

#endif // MENUACTIONRESULT_HPP
