#include "filetreefilter.hpp"

#include <QFileSystemModel>
#include <halgorithm/all.hpp>

namespace wgt {
void FileTreeFilter::addBannedExtensions(std::vector<QString> extensions) {
    bannedFiles.insert(extensions.begin(), extensions.end());
}

bool FileTreeFilter::filterAcceptsRow(
    int                sourceRow,
    const QModelIndex& sourceParent) const {
    QModelIndex itemIndex = sourceModel()->index(
        sourceRow, 0, sourceParent);

    QFileSystemModel* sourceFileModel = qobject_cast<QFileSystemModel*>(
        this->sourceModel());

    QFileInfo fileInfo  = sourceFileModel->fileInfo(itemIndex);
    QString   extension = fileInfo.suffix();

    if (bannedFiles.find(extension) != bannedFiles.end()) {
        return invertBanned;
    } else if (fileInfo.isFile()) {
        return !invertBanned;
    }

    return true;
}

bool FileTreeFilter::isInvertedBanned() const {
    return invertBanned;
}

void FileTreeFilter::setInvertBanned(bool value) {
    invertBanned = value;
}
} // namespace wgt
