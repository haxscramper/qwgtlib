#ifndef UISEPARATOR_H
#define UISEPARATOR_H

#include <QFrame>

namespace wgt {
class UISeparator : public QFrame
{
  public:
    UISeparator(QWidget* parent = nullptr);
};
} // namespace wgt

#endif // UISEPARATOR_H
