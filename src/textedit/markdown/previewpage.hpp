#ifndef PREVIEWPAGE_H
#define PREVIEWPAGE_H

#include <QWebEnginePage>

namespace wgt {
class PreviewPage : public QWebEnginePage
{
    Q_OBJECT
  public:
    explicit PreviewPage(QObject* parent = nullptr)
        : QWebEnginePage(parent) {
    }

  protected:
    bool acceptNavigationRequest(
        const QUrl&    url,
        NavigationType type,
        bool           isMainFrame);
};
} // namespace wgt

#endif // PREVIEWPAGE_H
