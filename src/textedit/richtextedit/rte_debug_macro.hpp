#ifndef RTE_DEBUG_MACRO_HPP
#define RTE_DEBUG_MACRO_HPP

#include <hdebugmacro/all.hpp>


#ifdef RTEWIDGET_DEBUG
#    define RTE_LOG DebugLogger::log()
#    define RTE_FUNC_BEGIN DEBUG_FUNCTION_BEGIN
#    define RTE_FUNC_END DEBUG_FUNCTION_END
#    define RTE_FUNC_RET(message) DEBUG_FUNCTION_PRE_RETURN(message)
#else
#    define RTE_LOG VOID_LOG
#    define RTE_FUNC_BEGIN
#    define RTE_FUNC_END
#    define RTE_FUNC_RET(message)
#endif

#endif // RTE_DEBUG_MACRO_HPP
