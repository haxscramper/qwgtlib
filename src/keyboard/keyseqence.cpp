#include "keyseqence.hpp"

namespace wgt {
KeySeqence::KeySeqence() {
    ctrl  = false;
    shift = false;
    meta  = false;
    alt   = false;
}

KeySeqence::KeySeqence(QKeyEvent* keyEvent) {
    QString sequence;
    ctrl  = keyEvent->modifiers() & Qt::ControlModifier;
    shift = keyEvent->modifiers() & Qt::ShiftModifier;
    alt   = keyEvent->modifiers() & Qt::AltModifier;
    meta  = keyEvent->modifiers() & Qt::MetaModifier;
    key   = QKeySequence(keyEvent->key()).toString();

    ROW() << "KeySequence";
    ROW() << "ctrl";
    ROW(1) << (ctrl ? dbg::Background::Green : dbg::Background::Red);
    ROW() << "shift";
    ROW(1) << (shift ? dbg::Background::Green : dbg::Background::Red);
    ROW() << "alt";
    ROW(1) << (alt ? dbg::Background::Green : dbg::Background::Red);
    ROW() << "win";
    ROW(1) << (meta ? dbg::Background::Green : dbg::Background::Red);
    ROW() << "Key";
    ROW(1) << key;
}

KeySeqence::KeySeqence(QString sequence) {
    sequence         = sequence.toLower();
    QStringList keys = sequence.split(delimiter);
    shift            = keys.contains("shift");
    ctrl             = keys.contains("ctrl");
    alt              = keys.contains("alt");
    meta             = keys.contains("win");
    key              = keys.last();
}

bool KeySeqence::operator<(const KeySeqence& other) const {
    if (ctrl != other.ctrl) {
        return ctrl < other.ctrl;

    } else if (shift != other.shift) {
        return shift < other.shift;

    } else if (alt != other.alt) {
        return alt < other.alt;

    } else if (meta != other.meta) {
        return meta < other.meta;
    }

    return key.toLower() < other.key.toLower();
}

bool KeySeqence::operator==(const KeySeqence& other) const {
    return (
        this->shift == other.shift && this->ctrl == other.ctrl
        && this->alt == other.alt && this->meta == other.meta
        && this->key.toLower() == other.key.toLower());
}

QString KeySeqence::toString() const {
    QString result;
    result += ctrl ? "ctrl+" : "";
    result += shift ? "shift+" : "";
    result += alt ? "alt+" : "";
    result += meta ? "win+" : "";
    result += key;
    return result.toLower();
}
} // namespace wgt
