#ifndef RTE_SUPPORT_HPP
#define RTE_SUPPORT_HPP

#include <QBrush>
#include <QColor>
#include <QTextBlock>
#include <QTextCursor>
#include <QTextTable>
#include <QTextTableCell>
#include <QTextTableFormat>

namespace spt {
void setTableCellColor(QColor color, QTextCursor cursor);
}

#endif // RTE_SUPPORT_HPP
