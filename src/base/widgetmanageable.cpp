#include "widgetmanageable.hpp"

namespace wgtm {
void WidgetManageable::addSupported(const QString& extension) {
    if (!supportedExtensions.contains(extension)) {
        supportedExtensions.push_back(extension);
    }
}


/*!
 * \brief Check whether file is supported natively by widget
 * \param Extension file suffix
 * \return bool
 */
bool WidgetManageable::isSupported(const QString& extension) {
    return supportedExtensions.contains(extension);
}


/*!
 * \brief Check whether file possibly be opened with this widget
 * \param FileType file type (text, image etc)
 * \return bool
 */
bool WidgetManageable::isAllowed(const QString fileType) {
    return allowedFileTypes.contains(fileType);
}


/// \brief Add additional allowed file types specifically for this
/// widget instance \param fileType File type (text, image etc)
void WidgetManageable::addAllowed(const QString fileType) {
    if (!allowedFileTypes.contains(fileType)) {
        allowedFileTypes.push_back(fileType);
    }
}


bool WidgetManageable::isEmpty() const {
    return currentFile.isEmpty();
}


void WidgetManageable::debugName() {
}

QString WidgetManageable::getWidgetName() const {
    return "WidgetManageable";
}

QString WidgetManageable::getFileName() {
    QFileInfo file(currentFile);
    return file.baseName() + "." + file.suffix();
}


/// \brief Delete file and dump DataEntity instead
void WidgetManageable::overrideFile(QString path) {
    BASE_CLASS_UNUSED(path);
}


//! \brief Export current data into file
void WidgetManageable::writeFile(QFile& file) {
    Q_UNUSED(file);
}

void WidgetManageable::writeFile(QString path) {
    BASE_CLASS_UNUSED(path);
}


//! \brief Read data from file and set it as content for
//! widgetManageable
void WidgetManageable::readFile(QFile& file) {
    BASE_CLASS_UNUSED(file);
    DEBUG_BASE_CLASS_FUNCTION_WARNING
}


//! \brief Set set content's value usin data from UI. Usually followed
//! by saveFile
void WidgetManageable::updateContent() {
    DEBUG_BASE_CLASS_FUNCTION_WARNING
}


//! \brief Populate UI elements using data form stored content values
void WidgetManageable::updateUI() {
    DEBUG_BASE_CLASS_FUNCTION_WARNING
}


/// \return True if content of the widget has been modified or
/// currentFile us empty
bool WidgetManageable::requiresSaving() const {
    DEBUG_BASE_CLASS_FUNCTION_WARNING
    return isModified;
}


/// \brief
QString WidgetManageable::getExtension() const {
    DEBUG_BASE_CLASS_FUNCTION_WARNING
    return "";
}

Workspace* WidgetManageable::getParentWorkspace() const {
    return parentWorkspace;
}

void WidgetManageable::setParentWorkspace(Workspace* value) {
    parentWorkspace = value;
}


WidgetManageable::WidgetManageable() {
    parentWorkspace = nullptr;
}


WidgetManageable::~WidgetManageable() {
}


/// \brief WidgetManageable::getFile
/// \return QString Absolute path to currently opened file
QString WidgetManageable::getFile() {
    return currentFile;
}


/*!
 * \brief Save currently opened file.
 *
 * Write current file on disk. If current file was
 * not read from drive i.e. createn using "new" file or similar
 * `saveFileAs` will be called
 */
void WidgetManageable::saveFile() {
    QFile file(currentFile);
    if (file.exists()) {
        writeFile(file);
    } else {
        saveFileAs();
    }
}


// DOC
void WidgetManageable::newFile() {
}


/// \brief Save current file clear buffer. Set currentFile as path
void WidgetManageable::newFile(QString path) {
    BASE_CLASS_UNUSED(path)
}


/*!
 * \brief Ask user to provide new name for file and save under that
 * name
 */
void WidgetManageable::saveFileAs() {
    currentFile = askPath();
    QFile file(currentFile);
    writeFile(file);
}


void WidgetManageable::openFile(QString path) {
    currentFile = path;
}


/// \brief Ask path to save current file in target folder
/// \return Path to selected file or empty string if no file has been
/// selected
QString WidgetManageable::askPath(
    QString     folder,
    QStringList acceptedExtensions) {
    QWidget tempWidget;

    QFileDialog dialog(
        &tempWidget,
        this->getFile(),
        folder,
        spt::extensionsToFilter(acceptedExtensions));

    dialog.setDirectory(folder);
    dialog.setFileMode(QFileDialog::AnyFile);
    dialog.setViewMode(QFileDialog::Detail);
    dialog.setAcceptMode(QFileDialog::AcceptSave);
    dialog.setWindowFlag(Qt::WindowStaysOnTopHint);
    dialog.raise();
    QStringList fileNames;
    if (dialog.exec()) {
        fileNames = dialog.selectedFiles();
    }

    // FIX only one file should be selected at a time
    if (fileNames.isEmpty()) {
        return "";
    } else if (QFileInfo(fileNames.at(0)).suffix().isEmpty()) {
        return fileNames.at(0) + "." + getExtension();
    } else {
        return fileNames.at(0);
    }
}


void WidgetManageable::saveTestFile(QString extension) {
    QFile file(QString("/home/maxim/Desktop/test.%1").arg(extension));
    this->writeFile(file);
}


void WidgetManageable::init() {
}
} // namespace wgtm
