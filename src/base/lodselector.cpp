#include "lodselector.hpp"
#include <hsupport/misc.hpp>

LODSelector::LODSelector(QWidget* parent) : QFrame(parent) {
    initUI();
    initSignals();
}

void LODSelector::initUI() {
    mainLayout     = new QHBoxLayout(this);
    LOD_cbox       = new QComboBox(this);
    UIDensity_cbox = new QComboBox(this);

    mainLayout->addWidget(LOD_cbox);
    mainLayout->addWidget(UIDensity_cbox);

    LOD_cbox->setToolTip("LOD level");
    UIDensity_cbox->setToolTip("UI density");

    LOD_cbox->addItems({"All", "Medium", "Minimal"});
    UIDensity_cbox->addItems({"Cramped", "Compact", "Maximized"});

    mainLayout->setContentsMargins(spt::zeroMargins());
    this->setContentsMargins(spt::equalMargins(2));
    mainLayout->setSpacing(2);
}

void LODSelector::initSignals() {
    connect(
        LOD_cbox,
        &QComboBox::currentTextChanged,
        [this](const QString& text) {
            if (text == "All") {
                emit LODChanged(LODSupport::LODLevel::All);
            } else if (text == "Minimal") {
                emit LODChanged(LODSupport::LODLevel::Minimal);
            } else if (text == "Medium") {
                emit LODChanged(LODSupport::LODLevel::Medium);
            }
        });

    connect(
        UIDensity_cbox,
        &QComboBox::currentTextChanged,
        [this](const QString& text) {
            if (text == "Cramped") {
                emit UIDensityChanged(LODSupport::UIDensity::Cramped);
            } else if (text == "Compact") {
                emit UIDensityChanged(LODSupport::UIDensity::Compact);
            } else if (text == "Maximized") {
                emit UIDensityChanged(LODSupport::UIDensity::Maximized);
            }
        });
}
