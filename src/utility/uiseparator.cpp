#include "uiseparator.hpp"

namespace wgt {
UISeparator::UISeparator(QWidget* parent) : QFrame(parent) {
    this->setFrameShape(QFrame::VLine);
    this->setFrameShadow(QFrame::Sunken);
}
} // namespace wgt
