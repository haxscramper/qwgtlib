#TEMPLATE = lib
#CONFIG += staticlib

include($$PWD/fileview/fileview.pri)
include($$PWD/pdfviewer/pdfviewer.pri)
include($$PWD/webview/webview.pri)
include($$PWD/toolbar/toolbar.pri)
include($$PWD/imageviewer/imageviewer.pri)

INCLUDEPATH *= $$PWD


## Support
#  LIBS += -L$$BUILD_DIR/support -lsupport

# QScintilla
#  LIBS += -L$$SOURCE_DIR -lqscintilla2_qt5

## Baseclasses
#  LIBS += -L$$BUILD_DIR/baseclasses -lbaseclasses


unix: PKGCONFIG += poppler-qt5
unix: CONFIG += link_pkgconfig
