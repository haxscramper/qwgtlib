#ifndef LATEXBUILDPREVIEW_HPP
#define LATEXBUILDPREVIEW_HPP

#include <QDir>
#include <QFileInfo>
#include <QProcess>
#include <QVBoxLayout>
#include <QWidget>

#include <halgorithm/qwidget_cptr.hpp>

#include "latexpreview.hpp"


namespace wgtm {
class PdfViewer;
}


namespace wgt {
class LatexBuildPreview : public LatexPreview
{
    Q_OBJECT
  public:
    explicit LatexBuildPreview(QWidget* parent = nullptr);
    void setSource(QString source) override;
    void build() override;
    void updateUI() override;

    void setBuildFolder(const QDir& value);
    void setHeadFile(const QFileInfo& value);
    void setTailFile(const QFileInfo& value);

  private:
    spt::qwidget_cptr<wgtm::PdfViewer> pdfViewer;
    QProcess*                          texBuild;

    QString   texSource;
    bool      scratchBuild = true;
    void      updatePath();
    QFileInfo sourceFile;
    QFileInfo resultFile;
    QString   getHead();
    QString   getTail();
    void      buildTex(QString path);
    void      openPDF(QString path);
    void      createSourceFile();

    QDir      buildFolder;
    QFileInfo headFile;
    QFileInfo tailFile;
};
} // namespace wgt

#endif // LATEXBUILDPREVIEW_HPP
