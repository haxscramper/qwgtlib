HEADERS *= \
    $$PWD/codetextfield.hpp \
    $$PWD/qscilexerqss.h \
    $$PWD/codeeditor.hpp \
    $$PWD/qsciwrapper.hpp

SOURCES *= \
    $$PWD/codeeditor.cpp \
    $$PWD/codetextfield.cpp \
    $$PWD/qscilexerqss.cpp \
    $$PWD/codeeditor_filework.cpp \
    $$PWD/qsciwrapper.cpp \
    $$PWD/qsciwrapper_textedit.cpp
