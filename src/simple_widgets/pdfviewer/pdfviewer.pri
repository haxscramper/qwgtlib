HEADERS *= \
    $$PWD/pdfviewer.hpp \
    $$PWD/renderedpdf.hpp

SOURCES *= \
    $$PWD/pdfviewer.cpp \
    $$PWD/renderedpdf.cpp
