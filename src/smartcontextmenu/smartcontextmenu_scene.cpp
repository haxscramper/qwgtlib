#include "smartcontextmenu_scene.hpp"
#include "smartcontextmenu.hpp"


namespace wgt {
SmartContextMenu_Scene::SmartContextMenu_Scene(QWidget* parent)
    : QWidget(parent) {
    view       = new QGraphicsView(this);
    scene      = new QGraphicsScene(this);
    mainLayout = new QHBoxLayout(this);
    mainLayout->addWidget(view, 1);
    view->setScene(scene);
    this->setAttribute(Qt::WA_TranslucentBackground);
    view->setAttribute(Qt::WA_TranslucentBackground);
    view->setStyleSheet("background: transparent");
    this->setContentsMargins(spt::zeroMargins());
    mainLayout->setContentsMargins(spt::zeroMargins());
}

SmartContextMenu_Scene::~SmartContextMenu_Scene() {
    qDeleteAll(radialButtons);
    qDeleteAll(regularButtons);
}

void SmartContextMenu_Scene::fromFile(QString path) {
    QFileInfo fileInfo(path);
    if (!fileInfo.exists()) {
        return;
    }

    settingsFolder = fileInfo.dir().path();

    json menuItems = jsonFromFile(path);

    radialFromJson(menuItems);
    arrangeRadialButtons();

    regularMenuFromJson(menuItems);
    arrangeRegularMenu();
}

QString SmartContextMenu_Scene::getSetingsFolder() const {
    return settingsFolder;
}

QSize SmartContextMenu_Scene::getContentSize() const {
    qreal regularMenuHeight = regularButtons.size()
                              * spt::iter_call(
                                    regularButtons.begin(),
                                    regularButtons,
                                    0,
                                    [](RegularMenuButton* button) {
                                        return button->boundingRect()
                                            .height();
                                    });

    qreal regularMenuWidth = regularButtons.size()
                             * spt::iter_call(
                                   regularButtons.begin(),
                                   regularButtons,
                                   0,
                                   [](RegularMenuButton* button) {
                                       return button->boundingRect()
                                           .width();
                                   });
    return QSize(
        (radialDiam * 0.55 + std::max(regularMenuWidth, radialDiam * 0.55))
            * 1.1,
        (radialDiam * 0.55 + std::max(regularMenuHeight, radialDiam * 0.55)
         + maxDiam)
            * 1.1);
}

QPoint SmartContextMenu_Scene::getCenterDeviation() const {
    return QPoint(radialDiam / 2, radialDiam / 2);
}

void SmartContextMenu_Scene::updateView() {
    QRectF sceneRect(QPoint(0, 0), view->size());
    view->updateScene({sceneRect});
}

void SmartContextMenu_Scene::updateRadialDiam() {
    maxDiam = spt::iter_call(
        std::max_element(
            radialButtons.begin(),
            radialButtons.end(),
            [](RadialButton const* first, RadialButton const* second) {
                return first->getButtonDiam() < second->getButtonDiam();
            }),
        radialButtons,
        0,
        [](RadialButton const* button) {
            return button->getButtonDiam();
        });

    radialDiam = radialDistances.back() * 2 + maxDiam;
}

void SmartContextMenu_Scene::arrangeRadialButtons() {
    SCM_FUNC_BEGIN

    //#    auto iter_lc =


    uint levelCount = spt::iter_call(
        std::max_element(
            radialButtons.begin(),
            radialButtons.end(),
            [](RadialButton const* first, RadialButton const* second) {
                return first->getLevelIndex() < second->getLevelIndex();
            }),
        radialButtons,
        0,
        [](RadialButton const* button) {
            return button->getLevelNumber();
        });


    std::vector<uint> buttonsPerLevel(levelCount);
    for (uint i = 0; i < buttonsPerLevel.size(); ++i) {
        auto iter_bc = spt::max_element_if(
            radialButtons,
            [&](RadialButton* first, RadialButton* second) {
                return first->getButtonIndex() < second->getButtonIndex();
            },
            [&](RadialButton* button) {
                return button->getLevelIndex() == i;
            });

        buttonsPerLevel[i] = iter_bc == radialButtons.end()
                                 ? 0
                                 : (*iter_bc)->getButtonIndex();
    }

    updateRadialDiam();
    qreal sceneCenterHeight = radialDiam / 2.0;
    qreal sceneCenterWidth  = radialDiam / 2.0;

    std::for_each(
        radialButtons.begin(),
        radialButtons.end(),
        [&](RadialButton* button) {
            button->setDCenter(sceneCenterWidth, sceneCenterHeight);
            button->setRadialDistance(
                radialDistances[button->getLevelIndex()]);
            button->setButtonsOnLevel(
                buttonsPerLevel[button->getLevelIndex()]);
            button->updatePos();
            scene->addItem(button);
        });

    SCM_FUNC_END
}

void SmartContextMenu_Scene::radialFromJson(json& menuItems) {
    json radialSettings = menuItems["radial-menu-settings"];

    for (json& action : menuItems["radial-menu"]) {
        RadialButton* button = new RadialButton(this);
        button->fromJson(action, radialSettings);
        radialButtons.push_back(button);
    }

    for (json& distance : menuItems["radial-menu-settings"]["distances"]) {
        radialDistances.push_back(distance);
    }
}

void SmartContextMenu_Scene::arrangeRegularMenu() {
    SCM_FUNC_BEGIN

    qreal sceneCenterHeight = radialDiam / 2.0;
    qreal sceneCenterWidth  = radialDiam / 2.0;

    qreal menuWidth = spt::iter_call(
        std::max_element(
            regularButtons.begin(),
            regularButtons.end(),
            [&](RegularMenuButton* lhs, RegularMenuButton* rhs) {
                return lhs->getWidth() < rhs->getWidth();
            }),
        regularButtons,
        0,
        [](RegularMenuButton* button) { return button->getWidth(); });

    std::for_each(
        regularButtons.begin(),
        regularButtons.end(),
        [&](RegularMenuButton* button) {
            button->setDCenter(sceneCenterWidth, sceneCenterHeight);
            button->setCenterDeviation(1.3 * maxDiam, 1.3 * maxDiam);
            button->setWidth(menuWidth);
            button->updatePos();
            scene->addItem(button);
        });

    SCM_FUNC_END
}

void SmartContextMenu_Scene::regularMenuFromJson(json& menuItems) {
    json regularMenuSettings = menuItems["regular-menu-settings"];

    for (json& action : menuItems["regular-menu"]) {
        RegularMenuButton* button = new RegularMenuButton(this);
        button->fromJson(action, regularMenuSettings);
        regularButtons.push_back(button);
    }
}


void SmartContextMenu_Scene::contextMenuButtonChosen(
    MenuActionResult result) {
    optionSelected = true;
    emit smartMenuOptionChosen(result);
}

void SmartContextMenu_Scene::mousePressEvent(QMouseEvent* event) {
    if (!optionSelected) {
        QPoint       globalPos   = this->mapToGlobal(event->pos());
        QWidget*     widgetUnder = QApplication::widgetAt(globalPos);
        QMouseEvent* eventCopy   = new QMouseEvent(*event);
        eventCopy->setLocalPos(widgetUnder->mapFromGlobal(globalPos));
        QApplication::instance()->postEvent(widgetUnder, eventCopy);
        this->setAttribute(Qt::WA_TransparentForMouseEvents, true);
        event->accept();
        emit emptySpacePressed(globalPos, eventCopy);
    }
    QWidget::mousePressEvent(event);
}
} // namespace wgt
