#ifndef TWOWAYBUTTON_HPP
#define TWOWAYBUTTON_HPP

/*!
 * \file twowaybutton.hpp
 * \brief
 */

//===   Qt    ===//
#include <QHBoxLayout>
#include <QPushButton>
#include <QWidget>

//===  Internal  ===//
#include <halgorithm/qwidget_cptr.hpp>


//#  ////////////////////////////////////


namespace wgt {
class TwoWayButton : public QWidget
{
    Q_OBJECT
  public:
    explicit TwoWayButton(QWidget* parent = nullptr);
    QPushButton* getLeftButton() const;
    QPushButton* getRightButton() const;

    void setLeftText(QString text);
    void setRightText(QString text);
    void setText(QString leftText, QString rightText);

  signals:
    void rightClicked();
    void leftClicked();


  public slots:

  private:
    void                           initUI();
    void                           initSignals();
    spt::qwidget_cptr<QPushButton> left;
    spt::qwidget_cptr<QPushButton> right;
    spt::qwidget_cptr<QHBoxLayout> mainLayout;
};
} // namespace wgt

#endif // TWOWAYBUTTON_HPP
