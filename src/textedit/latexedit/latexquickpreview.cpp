#include "latexquickpreview.hpp"

#include <hdebugmacro/all.hpp>

namespace wgt {
LatexQuickPreview::LatexQuickPreview(QWidget* parent)
    : LatexPreview(parent)
    , preview(new QWebEngineView(this))
//  ,
{
    layout->addWidget(preview);
}

void LatexQuickPreview::setSource(QString source) {
    QFile headFile(
        previewHeadTail.absoluteFilePath("quick_preview_head.html"));
    QFile tailFile(
        previewHeadTail.absoluteFilePath("quick_preview_tail.html"));

    headFile.open(QFile::ReadOnly);
    tailFile.open(QFile::ReadOnly);

    QString head = headFile.readAll();
    QString tail = tailFile.readAll();
    QString body = "<div>" + source + "</div>";
    preview->setHtml(head + body + tail);
}

void LatexQuickPreview::build() {
}
} // namespace wgt
