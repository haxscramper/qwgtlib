#ifndef KEYEVENT_HPP
#define KEYEVENT_HPP

#include "keyseqence.hpp"
#include <QEvent>
#include <QKeyEvent>
#include <QKeySequence>


namespace wgt {
/*!
 * KeyEvent represents single key (or combination key+modifiers) press.
 */
class KeyEvent
{
  public:
    KeyEvent();
    KeyEvent(QKeyEvent* event);
    KeySeqence getSequence();

  private:
    bool       topWidgetCanAccept; ///< \todo WTF?
    KeySeqence sequence;
};
} // namespace wgt

#endif // KEYEVENT_HPP
