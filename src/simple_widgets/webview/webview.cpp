#include "webview.hpp"

namespace wgtm {
WebView::WebView(QWidget* parent) : QWebEngineView(parent) {
}

void WebView::openFile(QString path) {
    this->load(QUrl::fromLocalFile(path));
    WidgetManageable::openFile(path);
}

void WebView::saveFile() {
}

QString WebView::getWidgetName() const {
    return "WebView";
}
} // namespace wgtm
