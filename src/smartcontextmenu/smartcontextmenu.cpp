#include "smartcontextmenu.hpp"

namespace wgt {
SmartContextMenu::SmartContextMenu(
    QString  layoutFilePath,
    QString  name,
    QWidget* parent)
    : QDialog(parent, Qt::FramelessWindowHint | Qt::Popup)
    , regularMenu(name, parent) {
    setAttribute(Qt::WA_TranslucentBackground);
    posChange = {0, 10};

    scene      = new SmartContextMenu_Scene(this);
    mainLayout = new QHBoxLayout;
    mainLayout->addWidget(scene, 1);
    this->setLayout(mainLayout);
    setContentsMargins(spt::zeroMargins());
    mainLayout->setContentsMargins(spt::zeroMargins());

    connect(
        scene,
        &SmartContextMenu_Scene::smartMenuOptionChosen,
        this,
        &SmartContextMenu::actionChosen);

    connect(
        scene,
        &SmartContextMenu_Scene::emptySpacePressed,
        this,
        &SmartContextMenu::emptyScenePressed);

    fromFile(layoutFilePath);
    resize(scene->getContentSize());
}

void SmartContextMenu::exec(QPoint pos) {
    startPos = pos;
    pos += posChange;
    pos -= scene->getCenterDeviation();
    move(pos.x(), pos.y());
    exec();
}


void SmartContextMenu::emptyScenePressed(
    QPoint       globalPos,
    QMouseEvent* event) {
    QWidget*     widgetUnder = QApplication::widgetAt(globalPos);
    QMouseEvent* eventCopy   = new QMouseEvent(*event);
    QApplication::instance()->postEvent(widgetUnder, eventCopy);
    emit emptySpacePressed(globalPos, event);

    event->accept();
    this->close();
}

int SmartContextMenu::exec() {
    return QDialog::exec();
}

void SmartContextMenu::setPosChange(const QPoint& value) {
    posChange = value;
}


void SmartContextMenu::fromFile(QString file) {
    scene->fromFile(file);
}

void SmartContextMenu::actionChosen(MenuActionResult args) {
    close();
    args.startPos = this->startPos;
    emit smartMenuOptionChosen(args);
}
} // namespace wgt
