#include "latexpreview.hpp"

#include <hdebugmacro/all.hpp>
#include <hsupport/misc.hpp>

namespace wgt {
LatexPreview::LatexPreview(QWidget* parent)
    : QWidget(parent), layout(new QVBoxLayout(this)) {
    setLayout(layout);
    layout->setContentsMargins(spt::zeroMargins());
    setContentsMargins(spt::zeroMargins());
}

void wgt::LatexPreview::setSource(QString source) {
    BASE_CLASS_UNUSED(source)
}

void wgt::LatexPreview::build() {
}

void LatexPreview::updateUI() {
}
} // namespace wgt
