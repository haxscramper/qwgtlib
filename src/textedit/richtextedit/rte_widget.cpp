#include "rte_widget.hpp"

namespace wgtm {
RTE_Widget::RTE_Widget(QString _iconDir, QWidget* parent)
    : QWidget(parent) {
    buttonHeight  = 30;
    buttonMinSize = QSize(buttonHeight, buttonHeight);
    iconSize      = QSize(28, 28);
    iconDir       = _iconDir;
    setupUI();
    setupSignals();
}


void RTE_Widget::resizeEvent(QResizeEvent* event) {
    toolbar->fitToSize(spt::scale(event->size(), 0.8));
    QWidget::resizeEvent(event);
}


void RTE_Widget::setupSignals() {
    connect(insertTable_tbtn, &QToolButton::clicked, [&]() {
        showTableDialog(insertTable_tbtn->pos());
    });
}


void RTE_Widget::showTableDialog(QPoint pos) {
    wgt::InsertTableDialog dialog;
    pos += QPoint(0, toolbar->height());
    connect(
        &dialog,
        &wgt::InsertTableDialog::tableSettingsChoosen,
        [&](int rows, int columns, QTextTableFormat tableFormat) {
            emit tableInsertionRequested(rows, columns, tableFormat);
        });
    dialog.move(mapToGlobal(pos));
    dialog.exec();
}
} // namespace wgtm
