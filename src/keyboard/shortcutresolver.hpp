#ifndef SHORTCUTRESOLVER_H
#define SHORTCUTRESOLVER_H

#include <QByteArray>
#include <QDebug>
#include <QPair>
#include <QString>
#include <QStringList>
#include <QVector>
#include <boost/lexical_cast.hpp>
#include <boost/property_tree/info_parser.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <functional>
#include <iostream>
#include <memory>
#include <string>
#include <typeinfo>
#include <utility>
#include <vector>

#include "kbd_debug_macro.hpp"
#include "keyseqence.hpp"


namespace wgt {
/*!
 * \brief ShortcutResolver class represents and mapping between shortcuts
 * and associated commands.
 */
class ShortcutResolver
{
    typedef boost::property_tree::ptree         Ptree;
    typedef std::pair<std::string, std::string> binding;

  public:
    ShortcutResolver();

    QString getCommand(std::string shortcut);
    QString getCommand(QString shortcut);
    QString getCommand(const KeySeqence& sequence);
    QVector<QPair<QString, QString>> getCompletion(std::string shortcut);
    void    addBinding(std::string shortcut, std::string action);
    void    addBinding(QString shortcut, QString action);
    void    addBinding(const char* shortcut, const char* action);
    QString standartiseShortcut(std::string& shortcut);
    QString standartiseShortcut(QString shortcut);
    void    printTree();

    Ptree* getKeymap() const;
    void   setKeymap(Ptree* value);

    QString getDelimiter() const;
    void    setDelimiter(const QString& value);

    QString toPath(QString shortcut);
    QString toPath(std::string shortcut);

  private:
    std::unique_ptr<Ptree> keymap;
    std::vector<binding>   getChildren(Ptree& tree);
    QString                delimiter;
};
} // namespace wgt

#endif // SHORTCUTRESOLVER_H
