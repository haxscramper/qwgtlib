#ifndef MULITSELECT_HPP
#define MULITSELECT_HPP

#include <QApplication>
#include <QComboBox>
#include <QDebug>
#include <QGridLayout>
#include <QLabel>
#include <QLineEdit>
#include <QListView>
#include <QPushButton>
#include <QStringList>
#include <QStringListModel>
#include <QToolButton>
#include <QVBoxLayout>
#include <QWidget>

#include <hdebugmacro/all.hpp>
#include <hsupport/tagset.hpp>
#include <variant>

namespace wgt {
class MultiSelect : public QWidget
{
    Q_OBJECT
  public:
    explicit MultiSelect(QWidget* parent = nullptr);
    void setTagsPool(spt::TagSet* value);
    void setLabel(QString labelString);
    void addTag(QString tag);


    void        setSelectedList(spt::TagSet tagList);
    spt::TagSet getSelectedList() const;

  signals:
    void newTag(QString);

    // items are pointers because there should be only one huge pool of
    // tags for application. QStringList models are per-widget because they
    // can have differet filters
  private:
    QLineEdit* lineEdit;

    QListView* selector;
    QListView* selected;

    spt::TagSet selectedList;

    spt::TagSet*     tagsPool = nullptr;
    QStringListModel tagsPoolModel;
    QStringListModel selectedModel;

    QGridLayout* mainLayout;
    QLabel*      label;
    QPushButton* addTagBtn;
    QToolButton* cancelTagBtn;


    void initUI();
    void initContent();
    void setupSignals();

    void updateUI();
};
} // namespace wgt
#endif // MULITSELECT_HPP
