#include "keyboardfilter.hpp"

namespace wgt {
KeyboardFilter::KeyboardFilter(QObject* parent) : QObject(parent) {
    targetWidget = dynamic_cast<KeyboardSupport*>(parent);
}

bool KeyboardFilter::eventFilter(QObject* watched, QEvent* event) {
    Q_UNUSED(watched);

    targetWidget->processKeyEvent(event);

    return false;
}

KeyboardSupport* KeyboardFilter::getTargetWidget() const {
    return targetWidget;
}

void KeyboardFilter::setTargetWidget(KeyboardSupport* value) {
    targetWidget = value;
}
} // namespace wgt
