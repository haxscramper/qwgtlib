#ifndef ADOCEDITOR_HPP
#define ADOCEDITOR_HPP

#include "../codeeditor.hpp"
#include "../textedit.hpp"
#include <QVBoxLayout>
#include <QWidget>
#include <halgorithm/qwidget_cptr.hpp>


namespace wgtm {
class ADOCEditor
    : extends QWidget
    , implements TextEdit
{
    Q_OBJECT
  public:
    explicit ADOCEditor(QWidget* parent = nullptr);

  private:
    spt::qwidget_cptr<QVBoxLayout> mainLayout;
    spt::qwidget_cptr<CodeEditor>  textEdit;

    //#= TextEdit interface
  public:
    virtual void setText(QString newText) override;
};
} // namespace wgtm

#endif // ADOCEDITOR_HPP
