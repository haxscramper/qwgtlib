#include "toolbarsimple.hpp"

ToolbarSimple::ToolbarSimple(QWidget* parent) : QWidget(parent) {
    layout = new QHBoxLayout(this);
    setLayout(layout);
    setContentsMargins(spt::zeroMargins());
    layout->setContentsMargins(spt::equalMargins(spacing));
    setSpacing(spacing);
}


void ToolbarSimple::addWidget(QWidget* widget, LODLevel importance) {
    layout->addWidget(widget);
    importanceMap[widget] = importance;
    LOD_LOG << "Widget size:" << size();
}


/*!
 * \brief Add widget to toolbar and set it's mininum size to minSize
 */
void ToolbarSimple::addWidget(
    QWidget* widget,
    QSize    minSize, ///< Widget's minSize will be set to this value
    QSize    maxSize, ///< If not defalut value ({-1,-1}) widget's maxSize
                      ///< will be set to this value
    LODLevel importance) {
    widget->setMinimumSize(minSize);
    if (maxSize.height() == -1 && maxSize.width() == -1) {
        widget->setMaximumSize(maxSize);
    }
    addWidget(widget, importance);
}


void ToolbarSimple::addSpacer(QSpacerItem* spacer) {
    layout->addItem(spacer);
}


void ToolbarSimple::removeWidget(QWidget* widget) {
    layout->removeWidget(widget);
    importanceMap.erase(importanceMap.find(widget));
}


void ToolbarSimple::setSpacing(uint spacing) {
    layout->setSpacing(spacing);
    layout->setContentsMargins(spt::equalMargins(spacing));
}


QSize ToolbarSimple::getTreshold(LODSupport::LODLevel lodLevel) {
    uint widgetCount = 0;
    uint width       = spt::accumulate_if(
        importanceMap.begin(),
        importanceMap.end(),
        0,
        [&](uint prev, std::pair<QWidget*, LODLevel> widget) -> uint {
            widgetCount++;
            LOD_LOG << "Minimum size:" << widget.first->minimumSize();
            LOD_LOG << "Actual size :" << widget.first->size();
            return prev + widget.first->minimumWidth();
        },
        [&](std::pair<QWidget*, LODLevel> widget) -> bool {
            return lodFits(widget.second, lodLevel);
        });


    auto iter = spt::max_element_if(
        importanceMap,
        [](std::pair<QWidget*, LODLevel> lhs,
           std::pair<QWidget*, LODLevel> rhs) -> bool {
            return lhs.first->minimumHeight() < rhs.first->minimumHeight();
        },
        [&](std::pair<QWidget*, LODLevel> widget) -> bool {
            return lodFits(widget.second, lodLevel);
        });

    uint height = spt::iter_call(
        iter,
        importanceMap,
        0,
        [](std::pair<QWidget*, LODLevel> widget) -> int {
            return widget.first->minimumHeight();
        });

    height += spacing * 2;
    width += (widgetCount + 1) * spacing;
    return QSize(width, height);
}

void ToolbarSimple::showEvent(QShowEvent* event) {
    QWidget::showEvent(event);
    updateTresholds();
    setMaximumHeight(getTreshold(LODLevel::All).height() + spacing * 2);
}


void ToolbarSimple::updateUIFromLOD() {
    LOD_FUNC_BEGIN

    switch (currentLOD) {
        case All: {
            for (auto widget : importanceMap) {
                widget.first->show();
            }
        } break;

        case Medium: {
            for (auto widget : importanceMap) {
                if (lodFits(widget.second, Medium)) {
                    widget.first->show();
                } else {
                    widget.first->hide();
                }
            }
        } break;

        case Minimal: {
            for (auto widget : importanceMap) {
                if (lodFits(widget.second, Minimal)) {
                    widget.first->show();
                } else {
                    widget.first->hide();
                }
            }
        } break;

        case Zero: {
            for (auto widget : importanceMap) {
                if (lodFits(widget.second, Zero)) {
                    widget.first->show();
                } else {
                    widget.first->hide();
                }
            }
        } break;
    }

    LOD_FUNC_END
}
