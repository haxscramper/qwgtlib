HEADERS *= \
    $$PWD/latexquickpreview.hpp \
    $$PWD/latexbuildpreview.hpp \
    $$PWD/latexpreview.hpp \
    $$PWD/latexeditor.hpp

SOURCES *= \
    $$PWD/latexquickpreview.cpp \
    $$PWD/latexbuildpreview.cpp \
    $$PWD/latexpreview.cpp \
    $$PWD/latexeditor.cpp
