HEADERS *= \
    $$PWD/richtexteditor.hpp \
    $$PWD/rte_textfield.hpp \
    $$PWD/rte_debug_macro.hpp \
    $$PWD/inserttabledialog.hpp \
    $$PWD/rte_widget.hpp \
    $$PWD/rte_support.hpp

SOURCES *= \
    $$PWD/richtexteditor.cpp \
    $$PWD/richtexteditor_controls.cpp \
    $$PWD/richtexteditor_ui.cpp \
    $$PWD/richtexteditor_misc.cpp \
    $$PWD/rte_textfield_mime.cpp \
    $$PWD/rte_textfield_cxtmenu.cpp \
    $$PWD/rte_textfield.cpp \
    $$PWD/rte_textfield_keyboard.cpp \
    $$PWD/rte_textfield_actions.cpp \
    $$PWD/inserttabledialog.cpp \
    $$PWD/rte_widget_setup_ui.cpp \
    $$PWD/rte_widget.cpp \
    $$PWD/rte_support.cpp
