#include "twowaybutton.hpp"
#include <hsupport/misc.hpp>

namespace wgt {
TwoWayButton::TwoWayButton(QWidget* parent)
    : QWidget(parent)
    , left(new QPushButton(this))
    , right(new QPushButton(this))
    , mainLayout(new QHBoxLayout(this))
//  ,
{
    initUI();
    initSignals();
}

QPushButton* TwoWayButton::getLeftButton() const {
    return left.get();
}

QPushButton* TwoWayButton::getRightButton() const {
    return right.get();
}

void TwoWayButton::setLeftText(QString text) {
    left->setText(text);
}

void TwoWayButton::setRightText(QString text) {
    right->setText(text);
}

void TwoWayButton::setText(QString leftText, QString rightText) {
    left->setText(leftText);
    right->setText(rightText);
}

void TwoWayButton::initUI() {
    mainLayout->addWidget(left);
    mainLayout->addWidget(right);
    this->setLayout(mainLayout);

    mainLayout->setSpacing(0);
    mainLayout->setContentsMargins(spt::zeroMargins());
    this->setContentsMargins(spt::zeroMargins());
}

void TwoWayButton::initSignals() {
    connect(left, &QPushButton::clicked, [this]() { emit leftClicked(); });
    connect(
        right, &QPushButton::clicked, [this]() { emit rightClicked(); });
}
} // namespace wgt
