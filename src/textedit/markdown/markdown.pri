HEADERS *= \
    $$PWD/markdowndocument.hpp \
    $$PWD/previewpage.hpp \
    $$PWD/markdowneditor.hpp

SOURCES *= \
    $$PWD/markdowndocument.cpp \
    $$PWD/previewpage.cpp \
    $$PWD/markdowneditor.cpp

RESOURCES *= \
    $$PWD/resources/markdowneditor.qrc

