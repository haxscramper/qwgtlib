#include "quicktextedit.hpp"

namespace wgtm {
QuickTextEdit::QuickTextEdit(QWidget* parent) : QWidget(parent) {
    initUI();

    connect(editor, &wgt::CodeTextfield::saveRequested, [&]() {
        this->saveRequested();
    });
}

void QuickTextEdit::setLabel(QString label) {
    widgetLabel->setText(label);
}

void QuickTextEdit::setText(QString text) {
    editor->setText(text);
}

QString QuickTextEdit::getText() {
    return editor->getDocumentText();
}

void QuickTextEdit::initUI() {
    mainLayout  = new QVBoxLayout;
    editor      = new wgt::CodeTextfield(this);
    widgetLabel = new QLabel(this);
    mainLayout->addWidget(widgetLabel);
    mainLayout->addWidget(editor);
    widgetLabel->setAlignment(Qt::AlignCenter);
    mainLayout->setContentsMargins(spt::zeroMargins());
    this->setContentsMargins(spt::zeroMargins());
    this->setLayout(mainLayout);
}
} // namespace wgtm
