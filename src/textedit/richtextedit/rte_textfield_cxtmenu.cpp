#include "rte_textfield.hpp"

namespace wgtm {
void RTE_Textfield::showContextMenu(const QPoint& pos) {
    SCM_LOG << "Context menu requested";
    wgt::SmartContextMenu contextMenu(
        contextMenuSettingsPath.absoluteFilePath(), "Actions", this);

    connect(
        &contextMenu,
        &wgt::SmartContextMenu::smartMenuOptionChosen,
        this,
        &RTE_Textfield::smartContextMenuActionChosen);

    connect(
        &contextMenu,
        &wgt::SmartContextMenu::emptySpacePressed,
        this,
        &RTE_Textfield::contextMenuEmptySpacePressed);

    contextMenu.exec(mapToGlobal(pos));
}

void RTE_Textfield::contextMenuEmptySpacePressed(
    QPoint       pressGlobalPos,
    QMouseEvent* event) {
    pressGlobalPos -= QPoint(0, 0);
    event->setLocalPos(mapFromGlobal(pressGlobalPos));
    //#    setCurrentIndex(indexAt(mapFromGlobal(pressGlobalPos)));
    RTE_Textfield::mousePressEvent(event);
}


void RTE_Textfield::smartContextMenuActionChosen(
    wgt::MenuActionResult result) {
    Q_UNUSED(result)
    wgt::KBDParameters parameters;
    parameters.target = this;
    if (executeCommand(result.name, parameters)) {
        return;
    }

    if (result.name == "set-cell-background") {
        QColorDialog colorDialog;
        colorDialog.setWindowFlag(Qt::WindowStaysOnTopHint);
        colorDialog.exec();
        QColor color = colorDialog.selectedColor();
        if (color.isValid()) {
            setTableCellColor(color);
        }
    }
}

void RTE_Textfield::mousePressEvent(QMouseEvent* event) {
    QTextEdit::mousePressEvent(event);
}
} // namespace wgtm
