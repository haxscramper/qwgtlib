#ifndef KEYBOARDFILTER_H
#define KEYBOARDFILTER_H

#include <QDebug>
#include <QKeyEvent>
#include <QObject>
#include <memory>

#include "keyboardsupport.hpp"

namespace wgt {
class KeyboardSupport;

/// Custom handler for keyboard input on widgets
class KeyboardFilter : public QObject
{
    Q_OBJECT
  public:
    explicit KeyboardFilter(QObject* parent = nullptr);
    virtual bool eventFilter(QObject* watched, QEvent* event) override;

    KeyboardSupport* getTargetWidget() const;
    void             setTargetWidget(KeyboardSupport* value);

  private:
    KeyboardSupport* targetWidget;
    //#    KeyboardSupport* targetWidget;
};
} // namespace wgt

#endif // KEYBOARDFILTER_H
