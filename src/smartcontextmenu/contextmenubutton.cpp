#include "contextmenubutton.hpp"
#include "smartcontextmenu_scene.hpp"

namespace wgt {
ContextMenuButton::ContextMenuButton(SmartContextMenu_Scene* _scene)
    : scene(_scene) {
    setAcceptHoverEvents(true);
}

QString ContextMenuButton::getActionName() const {
    return actionName;
}

void ContextMenuButton::setActionName(const QString& value) {
    actionName = value;
}

void ContextMenuButton::hoverEnterEvent(QGraphicsSceneHoverEvent* event) {
    hover = true;
    QGraphicsItem::hoverEnterEvent(event);
}

void ContextMenuButton::hoverLeaveEvent(QGraphicsSceneHoverEvent* event) {
    hover = false;
    QGraphicsItem::hoverLeaveEvent(event);
}

void ContextMenuButton::mousePressEvent(QGraphicsSceneMouseEvent* event) {
    MenuActionResult result;
    result.name = actionName;
    scene->contextMenuButtonChosen(result);
    QGraphicsItem::mousePressEvent(event);
}


void ContextMenuButton::setDCenter(qreal dx, qreal dy) {
    centerDx = dx;
    centerDy = dy;
}

void ContextMenuButton::setCenterDeviation(qreal dx, qreal dy) {
    centerDx += dx;
    centerDy += dy;
}
} // namespace wgt
