#include "rte_textfield.hpp"

namespace wgtm {

/*!
 * \brief If any selection is present make it bold
 * \param cycle - if true boldness will be switched
 * to opposite. If false text will be made bold
 * \note This functions does not start undo/redo block
 */
void RTE_Textfield::setTextBold(bool cycle) {
    RTE_FUNC_BEGIN

    QTextCursor cursor = textCursor();
    if (!cursor.hasSelection()) {
        RTE_FUNC_END
        return;
    }

    QTextCharFormat format = cursor.charFormat();
    if (format.fontWeight() != QFont::Normal && cycle) {
        RTE_LOG << "Setting font to normal";
        format.setFontWeight(QFont::Normal);
    } else {
        RTE_LOG << "Setting font to bold";
        format.setFontWeight(QFont::Bold);
    }

    cursor.mergeCharFormat(format);
    mergeCurrentCharFormat(format);
    setFocus(Qt::TabFocusReason);

    RTE_FUNC_END
}


/*!
 * \brief If any selection is present make it italic
 * \param cycle - if true italic will be switched
 * to opposite. If false text will be made italic
 * \note This functions does not start undo/redo block
 */
void RTE_Textfield::setTextItalic(bool cycle) {
    RTE_FUNC_BEGIN

    QTextCursor cursor = textCursor();
    if (!cursor.hasSelection()) {
        RTE_FUNC_END
        return;
    }

    QTextCharFormat format = cursor.charFormat();
    if (format.fontItalic() && cycle) {
        RTE_LOG << "Setting font to italic";
        format.setFontItalic(false);
    } else {
        RTE_LOG << "Setting font to normal";
        format.setFontItalic(true);
    }

    cursor.mergeCharFormat(format);
    mergeCurrentCharFormat(format);
    setFocus(Qt::TabFocusReason);

    RTE_FUNC_END
}


/*!
 * \brief If any selection is present make it underlined
 * \param cycle - if true underlined will be switched
 * to opposite. If false text will be made underlined
 * \note This functions does not start undo/redo block
 */
void RTE_Textfield::setTextUnderlined(bool cycle) {
    RTE_FUNC_BEGIN

    QTextCursor cursor = textCursor();
    if (!cursor.hasSelection()) {
        RTE_FUNC_END
        return;
    }

    QTextCharFormat format = cursor.charFormat();
    if (format.fontUnderline() && cycle) {
        RTE_LOG << "Setting font to normal";
        format.setFontUnderline(false);
    } else {
        RTE_LOG << "Setting font to underline";
        format.setFontUnderline(true);
    }

    cursor.mergeCharFormat(format);
    mergeCurrentCharFormat(format);

    RTE_FUNC_END
}


/*!
 * \brief If any selection is present make it strikethrough
 * \param cycle - if true strikethrough will be switched
 * to opposite. If false text will be made strikethrough
 * \note This functions does not start undo/redo block
 */
void RTE_Textfield::setTextStrikethrough(bool cycle) {
    RTE_FUNC_BEGIN

    QTextCursor cursor = textCursor();
    if (!cursor.hasSelection()) {
        RTE_FUNC_END
        return;
    }

    QTextCharFormat format = cursor.charFormat();
    if (format.fontStrikeOut() && cycle) {
        RTE_LOG << "Setting font to normal";
        format.setFontStrikeOut(false);
    } else {
        RTE_LOG << "Setting font to strikeout";
        format.setFontStrikeOut(true);
    }

    cursor.mergeCharFormat(format);
    mergeCurrentCharFormat(format);

    RTE_FUNC_END
}

void RTE_Textfield::insertTable(
    int              rows,
    int              columns,
    QTextTableFormat tableFormat) {
    textCursor().insertTable(rows, columns, tableFormat);
}

void RTE_Textfield::setTableCellColor(QColor color) {
    spt::setTableCellColor(color, textCursor());
}

void RTE_Textfield::insertHline() {
    textCursor().insertHtml("<hr>");
}

} // namespace wgtm
