HEADERS *= \
    $$PWD/filetree.hpp \
    $$PWD/filetreeview.hpp \
    $$PWD/filetreefilter.hpp

SOURCES *= \
    $$PWD/filetree.cpp \
    $$PWD/filetreeview.cpp \
    $$PWD/filetreefilter.cpp \
    $$PWD/filetree_misc.cpp

DISTFILES += \
    $$PWD/filetree_context_menu.json
