#include "multiselect.hpp"

namespace wgt {
MultiSelect::MultiSelect(QWidget* parent) : QWidget(parent) {
    this->initUI();
    this->initContent();
    this->setupSignals();
}


void MultiSelect::setTagsPool(spt::TagSet* value) {
    tagsPool = value;
    this->updateUI();
}


void MultiSelect::setLabel(QString labelString) {
    label->setText(labelString);
}


void MultiSelect::setSelectedList(spt::TagSet tagList) {
    selectedList = tagList;
    this->updateUI();
}

spt::TagSet MultiSelect::getSelectedList() const {
    return selectedList;
}


void MultiSelect::addTag(QString tag) {
    if (!tag.isEmpty() && !selectedList.contains(tag)) {
        selectedList.addTag(tag);
        this->updateUI();
        if (tagsPool != nullptr && !tagsPool->contains(tag)) {
            emit newTag(tag);
        }
    }
}


void MultiSelect::updateUI() {
    if (tagsPool != nullptr) {
        tagsPoolModel.setStringList(tagsPool->toStringList());
    }

    selector->setModel(&tagsPoolModel);
    selectedModel.setStringList(selectedList.toStringList());
    selected->setModel(&selectedModel);
}
} // namespace wgt
