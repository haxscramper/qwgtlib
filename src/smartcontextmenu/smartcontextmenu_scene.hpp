#ifndef SMARTCONTEXTMENU_SCENE_HPP
#define SMARTCONTEXTMENU_SCENE_HPP

#include "menuactionresult.hpp"
#include "radialbutton.hpp"
#include "regularmenubutton.hpp"
#include <QApplication>
#include <QDir>
#include <QGraphicsEllipseItem>
#include <QGraphicsItem>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QHBoxLayout>
#include <QSize>
#include <QTimer>
#include <QWidget>
#include <halgorithm/all.hpp>
#include <algorithm>
#include <cmath>
#include <hsupport/misc.hpp>
#include <vector>


namespace wgt {
class SmartContextMenu_Scene : public QWidget
{
    Q_OBJECT
  public:
    explicit SmartContextMenu_Scene(QWidget* parent = nullptr);
    ~SmartContextMenu_Scene();

    QGraphicsView*  view  = nullptr;
    QGraphicsScene* scene = nullptr;
    void            fromFile(QString path);
    QString         getSetingsFolder() const;
    QSize           getContentSize() const;
    QPoint          getCenterDeviation() const;
    void            updateView();

  private:
    std::vector<RadialButton*>      radialButtons;
    std::vector<RegularMenuButton*> regularButtons;
    std::vector<qreal>              radialDistances;

    QHBoxLayout* mainLayout;
    QString      settingsFolder;
    qreal        radialDiam     = 0;
    bool         optionSelected = false;
    uint         maxDiam;

    void updateRadialDiam();
    void arrangeRadialButtons();
    void radialFromJson(json& menuItems);
    void arrangeRegularMenu();
    void regularMenuFromJson(json& menuItems);


  signals:
    void smartMenuOptionChosen(MenuActionResult);
    void emptySpacePressed(QPoint globalPos, QMouseEvent* event);

  public slots:
    void contextMenuButtonChosen(MenuActionResult result);


    // QWidget interface
  protected:
    void mousePressEvent(QMouseEvent* event) override;
};
} // namespace wgt

#endif // SMARTCONTEXTMENU_SCENE_HPP
