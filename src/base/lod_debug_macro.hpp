#ifndef LOD_DEBUG_MACRO_HPP
#define LOD_DEBUG_MACRO_HPP

#include <hdebugmacro/all.hpp>


#ifdef LOD_DEBUG
#    define LOD_LOG LOG
#    define LOD_FUNC_BEGIN DEBUG_FUNCTION_BEGIN
#    define LOD_FUNC_END DEBUG_FUNCTION_END
#    define LOD_FUNC_RET(message) DEBUG_FUNCTION_PRE_RETURN(message)
#else
#    define LOD_LOG VOID_LOG
#    define LOD_INFO VOID_LOG
#    define LOD_FUNC_BEGIN
#    define LOD_FUNC_END
#    define LOD_FUNC_RET(message)
#endif

#endif // LOD_DEBUG_MACRO_HPP
