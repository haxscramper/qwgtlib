#!/usr/bin/env bash
mkdir -p build
cd build
qmake ../src/demo/main.pro CONFIG+=debug
make -j10 --quiet
cp --update demo ..
cd ..
