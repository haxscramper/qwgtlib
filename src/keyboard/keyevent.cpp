#include "keyevent.hpp"

namespace wgt {
KeyEvent::KeyEvent() {
    topWidgetCanAccept = false;
}

KeyEvent::KeyEvent(QKeyEvent* event) : sequence(event) {
    //#    sequence = KeySeqence(event);
    topWidgetCanAccept = false;
}

KeySeqence KeyEvent::getSequence() {
    return sequence;
}
} // namespace wgt
