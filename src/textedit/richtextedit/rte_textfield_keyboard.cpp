#include "rte_textfield.hpp"

namespace wgtm {
void RTE_Textfield::keyPressEvent(QKeyEvent* event) {
    wgt::KeyEvent keyEvent(event);
    processKeyEvent(keyEvent);
    QTextEdit::keyPressEvent(event);

    DEBUG_FINALIZE_WINDOW
}

bool RTE_Textfield::processKeyEvent(wgt::KeyEvent& event) {
    QString            command = shortcuts.getCommand(event.getSequence());
    wgt::KBDParameters parameters;
    parameters.target = this;

    return executeCommand(command, parameters);
}

void RTE_Textfield::initShortcuts() {
    shortcuts.addBinding("ctrl+b", "set-text-bold");
    keyboardActions["set-text-bold"] =
        [&](wgt::KBDParameters& internalParameters,
            json&               userParameters) -> bool {
        Q_UNUSED(internalParameters)
        Q_UNUSED(userParameters)
        this->setTextBold(true);
        return true;
    };

    shortcuts.addBinding("ctrl+i", "set-text-italic");
    keyboardActions["set-text-italic"] =
        [&](wgt::KBDParameters& internalParameters,
            json&               userParameters) -> bool {
        Q_UNUSED(internalParameters)
        Q_UNUSED(userParameters)
        this->setTextItalic(false);
        return true;
    };

    shortcuts.addBinding("ctrl+k", "set-text-strikethrough");
    keyboardActions["set-text-strikethrough"] =
        [&](wgt::KBDParameters& internalParameters,
            json&               userParameters) -> bool {
        Q_UNUSED(internalParameters)
        Q_UNUSED(userParameters)
        this->setTextStrikethrough(false);
        return true;
    };

    shortcuts.addBinding("ctrl+u", "set-text-underline");
    keyboardActions["set-text-underline"] =
        [&](wgt::KBDParameters& internalParameters,
            json&               userParameters) -> bool {
        Q_UNUSED(internalParameters)
        Q_UNUSED(userParameters)
        this->setTextUnderlined(false);
        return true;
    };
}

} // namespace wgtm
