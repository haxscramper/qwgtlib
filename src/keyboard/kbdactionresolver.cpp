#include "kbdactionresolver.hpp"

namespace wgt {
/// Associate keyboard action with it's name (`action.getName()`)
void KBDActionResolver::addAction(KeyboardAction action) {
    actionMap[action.getName()] = action;
}

/// Return true if keyboard action with given name can be found in the
/// list.
bool KBDActionResolver::hasCommand(QString command) {
    return (actionMap.find(command) != actionMap.end());
}

// FIXME there is something really strange about this
// implementation, I'm sure that it migth cause runtime errors if
// "index" is not in the map.
KeyboardAction& KBDActionResolver::operator[](QString index) {
    if (actionMap.find(index) != actionMap.end()) {
        actionMap[index].setName(index);
        return actionMap[index];
    } else {
        return actionMap[index];
    }
}
} // namespace wgt
