#include "inserttabledialog.hpp"

namespace wgt {
InsertTableDialog::InsertTableDialog(QWidget* parent)
    : QDialog(parent, Qt::FramelessWindowHint | Qt::Popup) {
    rowCount_label    = new QLabel(this);
    columnCount_label = new QLabel(this);
    rowCount_spbx     = new QSpinBox(this);
    columnCount_spbx  = new QSpinBox(this);
    accept_pbtn       = new QPushButton(this);
    cancel_pbtn       = new QPushButton(this);
    layout            = new QGridLayout(this);

    layout->addWidget(rowCount_label, 1, 1, 1, 1);
    layout->addWidget(rowCount_spbx, 2, 1, 1, 1);

    layout->addWidget(columnCount_label, 1, 2, 1, 1);
    layout->addWidget(columnCount_spbx, 2, 2, 1, 1);

    layout->addWidget(cancel_pbtn, 3, 1, 1, 1);
    layout->addWidget(accept_pbtn, 3, 2, 1, 1);

    rowCount_spbx->setRange(1, 100);
    columnCount_spbx->setRange(1, 100);

    this->setLayout(layout);

    accept_pbtn->setText("Accept");
    cancel_pbtn->setText("Cancel");

    rowCount_label->setText("Rows");
    columnCount_label->setText("Columns");

    rowCount_label->setAlignment(Qt::AlignCenter);
    columnCount_label->setAlignment(Qt::AlignCenter);

    connect(accept_pbtn, &QPushButton::clicked, [&]() {
        QTextTableFormat format;

        int rows    = rowCount_spbx->value();
        int columns = columnCount_spbx->value();

        format.setCellSpacing(0);

        emit tableSettingsChoosen(rows, columns, format);
        close();
    });


    connect(cancel_pbtn, &QPushButton::clicked, [&]() { close(); });
}
} // namespace wgt
