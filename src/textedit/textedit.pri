SOURCES *= \
    $$PWD/textedit.cpp \
    $$PWD/quicktextedit.cpp

HEADERS *= \
    $$PWD/textedit.hpp \
    $$PWD/quicktextedit.hpp \
    $$PWD/richtexteditor.hpp \
    $$PWD/latexeditor.hpp \
    $$PWD/codeeditor.hpp \
    $$PWD/rsteditor.hpp \
    $$PWD/adoceditor.hpp


include($$PWD/markdown/markdown.pri)
include($$PWD/richtextedit/richtextedit.pri)
include($$PWD/latexedit/latexedit.pri)
include($$PWD/codeeditor/codeeditor.pri)
include($$PWD/rsteditor/rsteditor.pri)
include($$PWD/adoceditor/adoceditor.pri)




