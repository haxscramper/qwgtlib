#ifndef LATEXPREVIEW_HPP
#define LATEXPREVIEW_HPP

#include <QVBoxLayout>
#include <QWidget>

#include <halgorithm/qwidget_cptr.hpp>

namespace wgt {
class LatexPreview : public QWidget
{
    Q_OBJECT
  public:
    explicit LatexPreview(QWidget* parent = nullptr);
    virtual void setSource(QString source);
    virtual void build();
    virtual void updateUI();

  protected:
    spt::qwidget_cptr<QVBoxLayout> layout;

  signals:
    void buildFinished();
    void buildStarted();
};
} // namespace wgt

#endif // LATEXPREVIEW_HPP
