#ifndef ACTIONRESOLVER_HPP
#define ACTIONRESOLVER_HPP

#include "keyboardaction.hpp"
#include <QHash>
#include <QString>
#include <functional>
#include <hdebugmacro/all.hpp>
#include <unordered_map>

namespace std {
template <>
struct hash<QString> {
    std::size_t operator()(const QString& s) const {
        return qHash(s);
    }
};
} // namespace std

namespace wgt {
// TODO class name ends on "er": this is either poorly named class or
// it's functionality can be replaced using free function and hash map
/*!

\brief Finding which command name is associated with wich keyboard
action. This class is likely to be \deprecated in the future and it's
functionality is going to be integrated in the KeyboardSupport class
directly.

 */
class KBDActionResolver
{
  public:
    void            addAction(KeyboardAction action);
    bool            hasCommand(QString command);
    KeyboardAction& operator[](QString index);

  private:
    std::unordered_map<QString, KeyboardAction> actionMap;
};
} // namespace wgt
#endif // ACTIONRESOLVER_HPP
