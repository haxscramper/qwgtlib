HEADERS *= \
    $$PWD/keyevent.hpp \
    $$PWD/keyseqence.hpp \
    $$PWD/keyboardfilter.hpp \
    $$PWD/keyboardsupport.hpp \
    $$PWD/shortcutresolver.hpp \
    $$PWD/keyboardaction.hpp \
    $$PWD/kbd_debug_macro.hpp \
    $$PWD/kbdactionresolver.hpp

SOURCES *= \
    $$PWD/keyevent.cpp \
    $$PWD/keyseqence.cpp \
    $$PWD/keyboardfilter.cpp \
    $$PWD/keyboardsupport.cpp \
    $$PWD/shortcutresolver.cpp \
    $$PWD/keyboardaction.cpp \
    $$PWD/kbdactionresolver.cpp


INCLUDEPATH *= $$PWD
