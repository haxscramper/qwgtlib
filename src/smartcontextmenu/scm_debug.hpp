#ifndef SCM_DEBUG_HPP
#define SCM_DEBUG_HPP

#include <hdebugmacro/all.hpp>

#define SCM_DEBUG

#ifdef SCM_DEBUG
#    define SCM_LOG LOG
#    define SCM_FUNC_BEGIN DEBUG_FUNCTION_BEGIN
#    define SCM_FUNC_END DEBUG_FUNCTION_END
#    define SCM_FUNC_RET(message) DEBUG_FUNCTION_PRE_RETURN(message)
#else
#    define SCM_LOG VOID_LOG
#    define SCM_FUNC_BEGIN
#    define SCM_FUNC_END
#    define SCM_FUNC_RET(message)
#endif

#endif // SCM_DEBUG_HPP
