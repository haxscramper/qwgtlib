#ifndef WIDGETINTERFACE_HPP
#define WIDGETINTERFACE_HPP

#include <QDir>

#define implements public
#define extends public

class WidgetInterface
{
  public:
    WidgetInterface()          = default;
    virtual ~WidgetInterface() = default;


    void setIconDir(const QDir& value);
    void setIconDir(QString absPath);
    QDir getIconDir() const;

  protected:
    virtual void initUI();
    virtual void initMenu();
    virtual void initControls();
    virtual void initContent();
    virtual void initActions();
    virtual void initSignals();
    virtual void updateUI();

    /*!
     * Directory that contains icons used in UI. Files that are contained
     * in the directory are specific for each widget (i.e. rich text editor
     * requires "strikethough.svg" image and should be located in the
     * folder specified by this variable)
     */
    QDir iconDir;
};

#endif // WIDGETINTERFACE_HPP
