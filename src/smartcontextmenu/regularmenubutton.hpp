#ifndef RECTMENUBUTTON_HPP
#define RECTMENUBUTTON_HPP

#include "contextmenubutton.hpp"
#include <QFont>
#include <QFontMetrics>

namespace wgt {
class RegularMenuButton : public ContextMenuButton
{
  public:
    RegularMenuButton(SmartContextMenu_Scene* _scene);
    qreal getWidth() const;
    void  setWidth(const qreal& value);

  private:
    QRectF  buttonRect;
    uint    buttonIndex;
    QString menuName;
    uint    textOffsetHorizontal = 2;
    qreal   textHeight;

    // ContextMenuButton interface
  public:
    void updatePos() override;
    void fromJson(json& buttonSettings, json& globalSettings) override;

    // QGraphicsItem interface
  public:
    QRectF boundingRect() const override;
    void   paint(
          QPainter*                       painter,
          const QStyleOptionGraphicsItem* option,
          QWidget*                        widget) override;
};
} // namespace wgt

#endif // RECTMENUBUTTON_HPP
