#ifndef KEYSEQENCE_HPP
#define KEYSEQENCE_HPP

#include <QDebug>
#include <QKeyEvent>
#include <QKeySequence>
#include <hdebugmacro/all.hpp>


namespace wgt {
/// Single keyb combination (such as ctrl+shift+t for example)
class KeySeqence
{
  public:
    KeySeqence();
    KeySeqence(QKeyEvent* keyEvent);
    KeySeqence(QString sequence);

    bool hasShift() const {
        return shift;
    }

    bool hasCtrl() const {
        return ctrl;
    }

    bool hasMeta() const {
        return meta;
    }

    bool hasAlt() const {
        return alt;
    }

    bool    operator<(const KeySeqence& other) const;
    bool    operator==(const KeySeqence& other) const;
    QString toString() const;

  private:
    bool    shift;
    bool    ctrl;
    bool    alt;
    bool    meta;
    QString key;
    QString delimiter = "+";
};
} // namespace wgt

#endif // KEYSEQENCE_HPP
