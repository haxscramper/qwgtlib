#pragma once

#include <QDialog>
#include <halgorithm/qwidget_cptr.hpp>

class QLineEdit;
class QHBoxLayout;

namespace wgt {
class StringPrompt : public QDialog
{
    Q_OBJECT

  public:
    StringPrompt(QWidget* parent = nullptr);
    void grabFocus();
    void selectAllUntil(QString delimiter);
    void setText(QString& text);


  signals:
    void stringSelected(QString res);


  private:
    spt::qwidget_cptr<QLineEdit>   lineEdit;
    spt::qwidget_cptr<QHBoxLayout> layout;
};
} // namespace wgt
