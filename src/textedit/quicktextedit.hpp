#ifndef QUICKTEXTEDIT_HPP
#define QUICKTEXTEDIT_HPP

#include "codeeditor.hpp"
#include <QLabel>
#include <QVBoxLayout>
#include <QWidget>

namespace wgtm {
/// \todo Replace text editor with quick text edit
/// \todo Implement quick text editing
class QuickTextEdit : public QWidget

{
    Q_OBJECT
  public:
    explicit QuickTextEdit(QWidget* parent = nullptr);
    wgtm::CodeEditor* getEditor();
    void              setLabel(QString label);
    void              setText(QString text);
    QString           getText();

  private:
    QLabel*             widgetLabel;
    wgt::CodeTextfield* editor;
    QVBoxLayout*        mainLayout;
    void                initUI();

  signals:
    void saveRequested();
};
} // namespace wgtm

#endif // QUICKTEXTEDIT_HPP
